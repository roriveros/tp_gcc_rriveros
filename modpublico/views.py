from django.shortcuts import render
from .forms import FormPublico
from django.views.generic import View
from modreserva.models import Reserva
from modrecurso.models import Recurso,TipoRecurso
from django.contrib import messages
from modmantenimiento.models import Mantenimiento
from datetime import date, datetime, time, timedelta
class estadisticas(View):
    def get(self, request, *args, **kwargs):
        print("Entro estadisticas")
        try:
            tipos=TipoRecurso.objects.all()
        except:
            messages.error(request, 'No hay tipo de recurso')
        ct=[]
        cinco_contador =[]
        r=[]
        cr=[]
        entro=0
        for i in range(0,15):
            actual=0
            for tipo in tipos:
                #actual = 0
                contador = 0
                #reservas = Reserva.objects.all()
                reservas = Reserva.objects.all().filter(tipo_recurso=tipo,fecha_reserva__gte=datetime.now().date() - timedelta(days =7))
                #print(reservas)
                #print("aca")
                for reserva in reservas:
                    if reserva.tipo_recurso == tipo:
                        contador=contador+1
                if contador > actual:
                    #print("entro")
                    actual=contador
                    cinco_contador.append(actual)
                    ct.append(tipo.nombre)
                    entro=entro+1
                    tipos = tipos.exclude(pk=tipo.id)
        if entro<15:
            for j in range(entro,15):
                ct.append(None)
        print(cinco_contador)
        print(ct)
        print("Termina 1")
        #################################################

        try:
            recursos=Recurso.objects.all()
        except:
            messages.error(request, 'No hay recursos')
        c=0
        for tipo in tipos:
            cantidad = 0
            to_be_deleted=[]
            #print(tipo)
            for recurso in recursos:
                #print(recurso.tipo_recurso.nombre,"==",tipo.nombre)
                #print(recurso.tipo_recurso.nombre== tipo.nombre)
                if recurso.tipo_recurso.nombre==tipo.nombre:
                    cantidad=cantidad+1
                    to_be_deleted.append(recurso.id)
            #print(cantidad)
            c=c+1
            #print(c)
            cr.append(cantidad)
            r.append(tipo.nombre)
            #print(to_be_deleted)
            #print(recursos)
            recursos = recursos.exclude(id__in=to_be_deleted)
            #print(recursos)
        n=len(cr)
        #print("acaca")
        #print(cr)
        #print(r)
        if n < 10:
            for i in range(n,10):
                cr.append(None)
                r.append(None)
            #print("Termina 2")
        ######################################################
        m=[]
        cm=[]
        actual=0
        entro=0
        try:
            mantenimientos = Mantenimiento.objects.all()
            tipos=TipoRecurso.objects.all()
            #print(mantenimientos)
            # print(tipos)
        except:
            messages.error(request, 'No hay datos de mantenimiento')
        for i in range(0, 5):
            for tipo in tipos:
                actual=0
                contador = 0
                to_be_deleted = []
                for mantenimiento in mantenimientos:
                    #print(mantenimiento.recurso.tipo_recurso)
                    print(mantenimiento.recurso.tipo_recurso, "==" ,tipo)
                    print(mantenimiento.recurso.tipo_recurso.nombre == tipo.nombre)
                    if mantenimiento.recurso.tipo_recurso.nombre == tipo.nombre:
                        contador=contador+1
                        to_be_deleted.append(mantenimiento.id)
                if contador > actual:
                    actual = contador
                    cm.append(actual)
                    m.append(tipo.nombre)
                    mantenimientos = mantenimientos.exclude(id__in=to_be_deleted)
                    entro = entro + 1
                    #tipos = tipos.exclude(pk=tipo.id)
        if entro < 5:
            for j in range(entro, 5):
                cm.append(0)
                m.append(None)
        print(cm)
        print(m)
        #####################Cantidad actual disponible#################################
        ck=[]
        k=[]
        tipos=TipoRecurso.objects.all()
        for tipo in tipos:
            diponibles=Recurso.objects.all().filter(estado=Recurso.DISPONIBLE,tipo_recurso=tipo)
            n = len(diponibles)
            ck.append(n)
            k.append(tipo.nombre)

        print(ck)
        print(k)
        print("Termino estadisticaa")
        return render(request, 'modpublico/publico_creado.html', {'ck':ck,'k1': k[0], 'k2': k[1], 'k3': k[2], 'k4': k[3], 'k5': k[4],'k6': k[5], 'k7': k[6], 'k8': k[7], 'k9': k[8], 'k10': k[9],'k11': k[10], 'k12': k[11], 'k13': k[12], 'k14': k[13], 'k15': k[14],'cm':cm,'m1':m[0],'m2':m[1],'m3':m[2],'m4':m[3],'m5':m[4],'cinco_c': cinco_contador,'t1': ct[0], 't2': ct[1], 't3': ct[2], 't4': ct[3], 't5': ct[4],'t6': ct[5], 't7': ct[6], 't8': ct[7], 't9': ct[8], 't10': ct[9],'t11': ct[10], 't12': ct[11], 't13': ct[12], 't14': ct[13], 't15': ct[14],'cr':cr,'r1':r[0],'r2':r[1],'r3':r[2],'r4':r[3],'r5':r[4],'r6':r[5],'r7':r[6],'r8':r[7],'r9':r[8],'r10':r[9]})