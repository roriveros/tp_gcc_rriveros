from django import forms
from .models import Publico

class FormPublico(forms.ModelForm):
    """
         Formulario que se utiliza para la visualizacion de mantenimientos.
     """
    class Meta:
        model =Publico
        fields = ('tipo',)