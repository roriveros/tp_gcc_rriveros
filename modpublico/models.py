from django.db import models
from modrecurso.models import TipoRecurso,Recurso

class Publico(models.Model):
    """
        Modelo utilizado para la creacion de mantenimientos de recursos
    """
    tipo=models.CharField(max_length=20)

    def __str__(self):
        return self.tipo

