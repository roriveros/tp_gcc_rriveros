from django.conf.urls import url
from . import views
from .views import estadisticas

app_name = 'modpublico'

urlpatterns = [
    # crear mantenimiento
    # url(r'^crear_correctivo/$', views.crear_mantenimiento_correctivo, name='crear_mantenimiento_correctivo'),
    #url(r'^lista_publico/$', views.publico, name='lista_publico'),
    url(r'^lista_publico/$', estadisticas.as_view(), name='lista_publico'),
    # url(r'^modificar-(?P<parameter>\d+)/', views.modificar_mantenimiento, name='modificar_mantenimiento'),
    # # lista de mantenimientos
    # url(r'^listar/$', views.mantenimiento_list, name='mantenimiento_lista'),
    # # eliminar mantenimientos
    # url(r'^eliminar-(?P<parameter>[\w-]+).html', views.eliminar_mantenimiento, name='eliminar_mantenimiento'),
]
