from django.contrib.auth.models import User, Group, Permission
from modrecurso.models import Recurso, TipoRecurso
from modusuario.models import Area, Profile
from modmantenimiento.models import Mantenimiento
import datetime, pytz

# echo 'import myscript' | python manage.py shell

# python manage.py shell < datos.py


# Creacion de area
academico = Area.objects.create(nombre='ACADEMICO',
                                descripcion='Recursos academicos: silla, pupitre, mesa, aula normales')

informatica = Area.objects.create(nombre='INFORMATICA',
                                  descripcion='proyectores, notebook, microfono, parlante, pantalla de proyeccion, etc.')

laboratorio_de_informatica = Area.objects.create(nombre='LABORATORIO-INFORMATICA',
                                                 descripcion='aula, sillas, mesas, computadoras, pizarras, y todos los recursos informaticos en general')

laboratorio_de_fisica = Area.objects.create(nombre='LABORATORIO-FISICA',
                                            descripcion='aula, sillas, mesas, pizarras, y todos los recursos para experimientos en general')

laboratorio_de_electricidad = Area.objects.create(nombre='LABORATORIO-ELECTRICIDAD',
                                               descripcion='aula, sillas, mesas, pizarras, tableros electricos, etc.')

laboratorio_de_electrónica = Area.objects.create(nombre='LABORATORIO-ELECTRONICA',
                                                 descripcion='aula, sillas, mesas, pizarras, tableros electricos, etc.')

laboratorio_de_aeronautica = Area.objects.create(nombre='LABORATORIO-AERONAUTICA',
                                                 descripcion='aula, sillas, mesas, pizarras,componentes aeronaticos, etc.')

deportiva = Area.objects.create(nombre='DEPORTE', descripcion='polideportivo, pelotas, etc.')

cocina = Area.objects.create(nombre='COCINA', descripcion='elementos de cocina en general')


# Crear un TipoRecurso
proyector = TipoRecurso.objects.create(nombre='PROYECTOR',
                                       descripcion='aparato óptico que recibe una señal de vídeo y proyecta la imagen correspondiente en una pantalla de proyección 3000 lumenes c/hdmi, peso: 2kg, color: blanco')

mesa = TipoRecurso.objects.create(nombre='MESA',
                                  descripcion='un mueble compuesto por un mínimo de una tabla lisa que es sostenida por una o más patas')

mesa_inf = TipoRecurso.objects.create(nombre='MESA-INFORMATICA', descripcion='un mueble especial para computadoras')

notebook = TipoRecurso.objects.create(nombre='NOTEBOOK', descripcion='un tipo de computadora (ordenador) portátil')

aula = TipoRecurso.objects.create(nombre='AULA',
                                  descripcion='espacio donde se desarrolla el proceso de enseñanza-aprendizaje formal')

silla_clase = TipoRecurso.objects.create(nombre='SILLACLASE',
                                         descripcion='Asiento individual con patas,pupitre y respaldo')

silla_lab = TipoRecurso.objects.create(nombre='SILLALAB', descripcion='Asiento individual con patas y respaldo')

laboratorio_informatica = TipoRecurso.objects.create(nombre='LABORATORIO-INFORMATICA',
                                                     descripcion='lugar donde se prestan servicios de cómputo a los miembros de una comunidad o institución educativa')

laboratorio_elc = TipoRecurso.objects.create(nombre='LABORATORIO-ELECTRICIDAD',
                                             descripcion='dedicado al campo de la energía eléctrica, las máquinas y aparatos eléctricos')

laboratorio_electro = TipoRecurso.objects.create(nombre='LABORATORIO-ELECTRONICA',
                                                 descripcion='dedidado para experimentos con el servicio de impresión de tablillas de circuitos electrónicos')

laboratorio_fisica = TipoRecurso.objects.create(nombre='LABORATORIO-FISICA',
                                                descripcion='está orientado al aprendizaje de metodología de trabajo experimental y tratamiento de datos mediante la realización de un conjunto de prácticas sencillas que complementan aspectos fenomenológicos fundamentales en mecánica, electricidad y magnetismo discutidos en los cursos de Física General')

laboratorio_aeronatica = TipoRecurso.objects.create(nombre='LABORATORIO-AERONAUTICA',
                                                    descripcion='Sala limpia para ensayos, investigación y fabricación de componentes aeronáuticos')

cocina_elementos = TipoRecurso.objects.create(nombre='ELEMENTOS-COCINA',
                                              descripcion='Todo tipo de utensilios utilizados para la cocina')

circuitos_electricos = TipoRecurso.objects.create(nombre='CIRCUITO-ELECTRICO',
                                                    descripcion='red eléctrica que contiene al menos una trayectoria cerrada')
motor_digital = TipoRecurso.objects.create(nombre='ENTRENAMIENTO-MOTOR', descripcion='Sistema de indicación del motor completamente funcional, incluye la recolección de datos de dispositivos y displays digitales')

aula_magna = TipoRecurso.objects.create(nombre='AULA-MAGNA',
                                        descripcion='Aula de mayor tamaño e importancia, destinada generalmente a actos o ceremonias oficiales')
#Crear roles
permisos1 =(Permission.objects.filter(content_type=1))
permisos2 = Permission.objects.filter(content_type=2)
permisos3 = Permission.objects.filter(content_type=3)
permisos4 = Permission.objects.filter(content_type=4)
permisos5 = Permission.objects.filter(content_type=5)
permisos6 = Permission.objects.filter(content_type=6)
permisos7 = Permission.objects.filter(content_type=7)
permisos8 = Permission.objects.filter(content_type=8)
permisos9 = Permission.objects.filter(content_type=9)
permisos10 = Permission.objects.filter(content_type=10)
permisos11 = Permission.objects.filter(content_type=11)
permisos12 = Permission.objects.filter(content_type=12)
permisos13 = Permission.objects.filter(content_type=13)
permisos14 = Permission.objects.filter(content_type=14)
permisos15 = Permission.objects.filter(content_type=15)

rol01= Group.objects.create(name='Administrador del sistema')
rol01.permissions.set(permisos1 | permisos2 | permisos3 | permisos4 | permisos5 |
                               permisos6 | permisos7 | permisos8 | permisos9 | permisos10 |
                              permisos11 | permisos12 | permisos13 | permisos14 | permisos15)

rol02= Group.objects.create(name='Administrador general de recursos')
rol02.permissions.set(permisos1 | permisos5  | permisos10 |
                              permisos11 | permisos12 | permisos13 )
rol02.permissions.add(Permission.objects.get(id=45),Permission.objects.get(id=46),Permission.objects.get(id=47))

rol03= Group.objects.create(name='Administrador de usuarios')
rol03.permissions.set(permisos2 | permisos3 | permisos4 | permisos5 | permisos6 | permisos7 | permisos8)

rol04=Group.objects.create(name='Administrador de reservas')
rol04.permissions.set(permisos15)

rol05=Group.objects.create(name='Administrador general de mantenimientos')
rol05.permissions.set(permisos14 | permisos12)

rol05=Group.objects.create(name='Administrador de mantenimientos')
rol05.permissions.set(permisos14)
rol05.permissions.add(Permission.objects.get(id=41),Permission.objects.get(id=42),Permission.objects.get(id=43))

rol06= Group.objects.create(name='Administrador de recursos')
rol06.permissions.set(permisos1 | permisos5  | permisos10 |
                              permisos11 | permisos13 )
rol06.permissions.add(Permission.objects.get(id=34),Permission.objects.get(id=35),Permission.objects.get(id=36))
rol06.permissions.add(Permission.objects.get(id=45),Permission.objects.get(id=46),Permission.objects.get(id=47))

rol07= Group.objects.create(name='Usuario2')
rol07.permissions.add(Permission.objects.get(id=45),Permission.objects.get(id=46),Permission.objects.get(id=47))

# Crear usuarios

user00 = User.objects.create(username='admin', first_name='Ad', last_name='Min', email='admin@dubium.com', is_staff=True, is_superuser=True)
user00.set_password('admin')
user00.save()
user00.groups.add(rol01)


user01 = User.objects.create(username='Luis', first_name='Luis', last_name='Martinez', email='luis@hotmail.com')
user01.set_password('luis')
user01.save()
user01.groups.add(rol01)

user02 = User.objects.create(username='Maria', first_name='Maria', last_name='Martinez', email='maria@hotmail.com')
user02.set_password('maria')
user02.save()
user02.groups.add(rol02)

user03 = User.objects.create(username='Silvia', first_name='Silvia', last_name='Martinez', email='silvia@hotmail.com')
user03.set_password('silvia')
user03.save()
user03.groups.add(rol03)

user04 = User.objects.create(username='Pablo', first_name='Pablo', last_name='Martinez', email='pablo@hotmail.com')
user04.set_password('pablo')
user04.save()
user04.groups.add(rol04)

user05 = User.objects.create(username='Juan', first_name='Juan', last_name='Martinez', email='juan@hotmail.com')
user05.set_password('juan')
user05.save()
user05.groups.add(rol05)

user06 = User.objects.create(username='Carlos', first_name='Carlos', last_name='Martinez', email='carlos@hotmail.com')
user06.set_password('carlos')
user06.save()

user07 = User.objects.create(username='Camila', first_name='Camila', last_name='Martinez', email='camila@hotmail.com')
user07.set_password('camila')
user07.save()

user08 = User.objects.create(username='Sebastian', first_name='Sebastian', last_name='Martinez', email='sebastian@hotmail.com')
user08.set_password('sebastian')
user08.save()

user09 = User.objects.create(username='Andres', first_name='Andres', last_name='Martinez', email='andres@hotmail.com')
user09.set_password('andres')
user09.save()

user10 = User.objects.create(username='Enzo', first_name='Enzo', last_name='Martinez', email='enzo@hotmail.com')
user10.set_password('enzo')
user10.save()

profile0 = Profile.objects.create(user=user00, categoria=Profile.TITULAR, telefono='0985666770', ci='123456',
                                  direccion='Mi casa', area=academico)

profile1 = Profile.objects.create(user=user01, categoria=Profile.TITULAR, telefono='0985666777', ci='233566',
                                  direccion='San juan', area=academico)
profile1.tipo_recurso.add(mesa)

profile2 = Profile.objects.create(user=user02, categoria=Profile.TITULAR, telefono='0985666777', ci='233566',
                                  direccion='San juan', area=academico)
profile2.tipo_recurso.add(silla_clase)
profile3 = Profile.objects.create(user=user03, categoria=Profile.TITULAR, telefono='0985666777', ci='233566',
                                  direccion='San juan', area=academico)
profile3.tipo_recurso.add(aula)
profile4 = Profile.objects.create(user=user04, categoria=Profile.ENCARGADO, telefono='0985666777', ci='233566', direccion='San juan', area=informatica)
profile4.tipo_recurso.add(proyector)
profile5 = Profile.objects.create(user=user05, categoria=Profile.ADJUNTO, telefono='0985666777', ci='233566',
                                  direccion='San juan', area=laboratorio_de_informatica)
profile5.tipo_recurso.add(silla_lab)
profile6 = Profile.objects.create(user=user06, categoria=Profile.ASISTENTE, telefono='0985666777', ci='233566',
                                  direccion='San juan', area=laboratorio_de_informatica)
profile6.tipo_recurso.add(mesa_inf)
profile7 = Profile.objects.create(user=user07, categoria=Profile.FUNCIONARIO, telefono='0985666777', ci='233566',
                                  direccion='San juan', area=academico)
profile7.tipo_recurso.add(aula_magna)
profile8 = Profile.objects.create(user=user08, categoria=Profile.ENCARGADO, telefono='0985666777', ci='233566',
                                  direccion='San juan', area=cocina)
profile8.tipo_recurso.add(cocina_elementos)
profile9 = Profile.objects.create(user=user09, categoria=Profile.ALUMNO, telefono='0985666777', ci='233566',
                                  direccion='San juan', area=laboratorio_de_electricidad)
profile9.tipo_recurso.add(circuitos_electricos)
profile10 = Profile.objects.create(user=user10, categoria=Profile.ADJUNTO, telefono='0985666777', ci='233566',
                                   direccion='San juan', area=laboratorio_de_aeronautica)
profile10.tipo_recurso.add(motor_digital)

# #Crear Recursos

p1 = Recurso.objects.create(nombre='PROYECTOR01', estado=Recurso.DISPONIBLE, area=informatica, tipo_recurso=proyector)

p2 = Recurso.objects.create(nombre='PROYECTOR02', estado=Recurso.DESCOMPUESTO, area=informatica, tipo_recurso=proyector)

p3 = Recurso.objects.create(nombre='PROYECTOR03', estado=Recurso.DISPONIBLE, area=informatica, tipo_recurso=proyector)

p4 = Recurso.objects.create(nombre='PROYECTOR04', estado=Recurso.DISPONIBLE, area=informatica, tipo_recurso=proyector)

p5 = Recurso.objects.create(nombre='PROYECTOR05', estado=Recurso.DISPONIBLE, area=informatica, tipo_recurso=proyector)

p6 = Recurso.objects.create(nombre='PROYECTOR06', estado=Recurso.DISPONIBLE, area=informatica, tipo_recurso=proyector)

Recurso.objects.create(nombre='PROYECTOR07', estado=Recurso.DISPONIBLE, area=informatica, tipo_recurso=proyector)

Recurso.objects.create(nombre='PROYECTOR08', estado=Recurso.DISPONIBLE, area=informatica, tipo_recurso=proyector)

Recurso.objects.create(nombre='PROYECTOR09', estado=Recurso.DISPONIBLE, area=informatica, tipo_recurso=proyector)

Recurso.objects.create(nombre='PROYECTOR10', estado=Recurso.DISPONIBLE, area=informatica, tipo_recurso=proyector)

Recurso.objects.create(nombre='MESA01', estado=Recurso.DISPONIBLE, area=informatica, tipo_recurso=mesa)

Recurso.objects.create(nombre='MESA-INFORMATICA01', estado=Recurso.DISPONIBLE, area=informatica, tipo_recurso=mesa_inf)

Recurso.objects.create(nombre='AULA01', estado=Recurso.DISPONIBLE, area=informatica, tipo_recurso=aula)

Recurso.objects.create(nombre='AULA-MAGNA', estado=Recurso.DISPONIBLE, area=informatica, tipo_recurso=aula_magna)

# Laboratorios informatica
Recurso.objects.create(nombre='LABINF01', estado=Recurso.DISPONIBLE, area=laboratorio_de_informatica,
                       tipo_recurso=laboratorio_informatica)

Recurso.objects.create(nombre='LABINF02', estado=Recurso.DISPONIBLE, area=laboratorio_de_informatica,
                       tipo_recurso=laboratorio_informatica)

Recurso.objects.create(nombre='LABINF03', estado=Recurso.DISPONIBLE, area=laboratorio_de_informatica,
                       tipo_recurso=laboratorio_informatica)

Recurso.objects.create(nombre='LABINF04', estado=Recurso.DISPONIBLE, area=laboratorio_de_informatica,
                       tipo_recurso=laboratorio_informatica)

Recurso.objects.create(nombre='LABINF05', estado=Recurso.DISPONIBLE, area=laboratorio_de_informatica,
                       tipo_recurso=laboratorio_informatica)

Recurso.objects.create(nombre='LABINF06', estado=Recurso.DISPONIBLE, area=laboratorio_de_informatica,
                       tipo_recurso=laboratorio_informatica)

Recurso.objects.create(nombre='LABINF07', estado=Recurso.DISPONIBLE, area=laboratorio_de_informatica,
                       tipo_recurso=laboratorio_informatica)

Recurso.objects.create(nombre='LABINF08', estado=Recurso.DISPONIBLE, area=laboratorio_de_informatica,
                       tipo_recurso=laboratorio_informatica)

Recurso.objects.create(nombre='LABINF09', estado=Recurso.DISPONIBLE, area=laboratorio_de_informatica,
                       tipo_recurso=laboratorio_informatica)

Recurso.objects.create(nombre='LABINF10', estado=Recurso.DISPONIBLE, area=laboratorio_de_informatica,
                       tipo_recurso=laboratorio_informatica)

# Laboratorios electronica
Recurso.objects.create(nombre='LABELECTRO01', estado=Recurso.DISPONIBLE, area=laboratorio_de_electrónica,
                       tipo_recurso=laboratorio_electro)

Recurso.objects.create(nombre='LABELECTRO02', estado=Recurso.DISPONIBLE, area=laboratorio_de_electrónica,
                       tipo_recurso=laboratorio_electro)

Recurso.objects.create(nombre='LABELECTRO03', estado=Recurso.DISPONIBLE, area=laboratorio_de_electrónica,
                       tipo_recurso=laboratorio_electro)

Recurso.objects.create(nombre='LABELECTRO04', estado=Recurso.DISPONIBLE, area=laboratorio_de_electrónica,
                       tipo_recurso=laboratorio_electro)

Recurso.objects.create(nombre='LABELECTRO05', estado=Recurso.DISPONIBLE, area=laboratorio_de_electrónica,
                       tipo_recurso=laboratorio_electro)

Recurso.objects.create(nombre='LABELECTRO06', estado=Recurso.DISPONIBLE, area=laboratorio_de_electrónica,
                       tipo_recurso=laboratorio_electro)

Recurso.objects.create(nombre='LABELECTRO07', estado=Recurso.DISPONIBLE, area=laboratorio_de_electrónica,
                       tipo_recurso=laboratorio_electro)

Recurso.objects.create(nombre='LABELECTRO08', estado=Recurso.DISPONIBLE, area=laboratorio_de_electrónica,
                       tipo_recurso=laboratorio_electro)

Recurso.objects.create(nombre='LABELECTRO09', estado=Recurso.DISPONIBLE, area=laboratorio_de_electrónica,
                       tipo_recurso=laboratorio_electro)

Recurso.objects.create(nombre='LABELECTRO10', estado=Recurso.DISPONIBLE, area=laboratorio_de_electrónica,
                       tipo_recurso=laboratorio_electro)

# Laboratorios electricidad
Recurso.objects.create(nombre='LABELC01', estado=Recurso.DISPONIBLE, area=laboratorio_de_electricidad,
                       tipo_recurso=laboratorio_elc)

Recurso.objects.create(nombre='LABELC02', estado=Recurso.DISPONIBLE, area=laboratorio_de_electricidad,
                       tipo_recurso=laboratorio_elc)

Recurso.objects.create(nombre='LABELC03', estado=Recurso.DISPONIBLE, area=laboratorio_de_electricidad,
                       tipo_recurso=laboratorio_elc)

Recurso.objects.create(nombre='LABELC04', estado=Recurso.DISPONIBLE, area=laboratorio_de_electricidad,
                       tipo_recurso=laboratorio_elc)

Recurso.objects.create(nombre='LABELC05', estado=Recurso.DISPONIBLE, area=laboratorio_de_electricidad,
                       tipo_recurso=laboratorio_elc)

Recurso.objects.create(nombre='LABELC06', estado=Recurso.DISPONIBLE, area=laboratorio_de_electricidad,
                       tipo_recurso=laboratorio_elc)

Recurso.objects.create(nombre='LABELC07', estado=Recurso.DISPONIBLE, area=laboratorio_de_electricidad,
                       tipo_recurso=laboratorio_elc)

Recurso.objects.create(nombre='LABELC08', estado=Recurso.DISPONIBLE, area=laboratorio_de_electricidad,
                       tipo_recurso=laboratorio_elc)

Recurso.objects.create(nombre='LABELC09', estado=Recurso.DISPONIBLE, area=laboratorio_de_electricidad,
                       tipo_recurso=laboratorio_elc)

Recurso.objects.create(nombre='LABELC10', estado=Recurso.DISPONIBLE, area=laboratorio_de_electricidad,
                       tipo_recurso=laboratorio_elc)

# Laboratorios de fisica
Recurso.objects.create(nombre='LABFIS01', estado=Recurso.DISPONIBLE, area=laboratorio_de_fisica,
                       tipo_recurso=laboratorio_fisica)

Recurso.objects.create(nombre='LABFIS02', estado=Recurso.DISPONIBLE, area=laboratorio_de_fisica,
                       tipo_recurso=laboratorio_fisica)

Recurso.objects.create(nombre='LABFIS03', estado=Recurso.DISPONIBLE, area=laboratorio_de_fisica,
                       tipo_recurso=laboratorio_fisica)

Recurso.objects.create(nombre='LABFIS04', estado=Recurso.DISPONIBLE, area=laboratorio_de_fisica,
                       tipo_recurso=laboratorio_fisica)

Recurso.objects.create(nombre='LABFIS05', estado=Recurso.DISPONIBLE, area=laboratorio_de_fisica,
                       tipo_recurso=laboratorio_fisica)

Recurso.objects.create(nombre='LABFIS06', estado=Recurso.DISPONIBLE, area=laboratorio_de_fisica,
                       tipo_recurso=laboratorio_fisica)

Recurso.objects.create(nombre='LABFIS07', estado=Recurso.DISPONIBLE, area=laboratorio_de_fisica,
                       tipo_recurso=laboratorio_fisica)

Recurso.objects.create(nombre='LABFIS08', estado=Recurso.DISPONIBLE, area=laboratorio_de_fisica,
                       tipo_recurso=laboratorio_fisica)

Recurso.objects.create(nombre='LABFIS09', estado=Recurso.DISPONIBLE, area=laboratorio_de_fisica,
                       tipo_recurso=laboratorio_fisica)

Recurso.objects.create(nombre='LABFIS10', estado=Recurso.DISPONIBLE, area=laboratorio_de_fisica,
                       tipo_recurso=laboratorio_fisica)

# Crear mantenimientos
Mantenimiento.objects.create(recurso=p1, fecha_hora_inicio=pytz.utc.localize(
    datetime.datetime.strptime('25032017040506', '%d%m%Y%H%M%S')), fecha_hora_fin=pytz.utc.localize(
    datetime.datetime.strptime('31052017050506', '%d%m%Y%H%M%S')),
                             motivo='mantenimiento preventivo',
                             descripcion='un mantenimiento a un proyector', costo='5000', tipo=Mantenimiento.PREVENTIVO,
                             resultado=Mantenimiento.DISPONIBLE, estado=Mantenimiento.CONFIRMADO)

Mantenimiento.objects.create(recurso=p2, fecha_hora_inicio=pytz.utc.localize(
    datetime.datetime.strptime('25032017040506', '%d%m%Y%H%M%S')),
                             motivo='mantenimiento preventivo',
                             descripcion='lampara descompuesta', tipo=Mantenimiento.CORRECTIVO, estado=Mantenimiento.CONFIRMADO)

Mantenimiento.objects.create(recurso=p3, fecha_hora_inicio=pytz.utc.localize(
    datetime.datetime.strptime('25032017040506', '%d%m%Y%H%M%S')), fecha_hora_fin=pytz.utc.localize(
    datetime.datetime.strptime('31052017050506', '%d%m%Y%H%M%S')),
                             motivo='mantenimiento preventivo',
                             descripcion='un mantenimiento a un proyector', costo='5000', tipo=Mantenimiento.PREVENTIVO, estado=Mantenimiento.CONFIRMADO, resultado=Mantenimiento.DISPONIBLE)

Mantenimiento.objects.create(recurso=p4, fecha_hora_inicio=pytz.utc.localize(
    datetime.datetime.strptime('25032017040506', '%d%m%Y%H%M%S')), fecha_hora_fin=pytz.utc.localize(
    datetime.datetime.strptime('31052017050506', '%d%m%Y%H%M%S')),
                             motivo='puertos en mal estado',
                             descripcion='un mantenimiento a un proyector', costo='5000', tipo=Mantenimiento.PREVENTIVO,
                             resultado=Mantenimiento.DISPONIBLE, estado=Mantenimiento.CONFIRMADO)

Mantenimiento.objects.create(recurso=p5, fecha_hora_inicio=pytz.utc.localize(
    datetime.datetime.strptime('25032017040506', '%d%m%Y%H%M%S')), fecha_hora_fin=pytz.utc.localize(
    datetime.datetime.strptime('31052017050506', '%d%m%Y%H%M%S')),
                             motivo='mantenimiento preventivo',
                             descripcion='un mantenimiento a un proyector', costo='5000', tipo=Mantenimiento.PREVENTIVO,
                             resultado=Mantenimiento.DISPONIBLE, estado=Mantenimiento.CONFIRMADO)

Mantenimiento.objects.create(recurso=p6, fecha_hora_inicio=pytz.utc.localize(
    datetime.datetime.strptime('25032017040506', '%d%m%Y%H%M%S')), fecha_hora_fin=pytz.utc.localize(
    datetime.datetime.strptime('31052017050506', '%d%m%Y%H%M%S')),
                             motivo='mantenimiento preventivo',
                             descripcion='un mantenimiento a un proyector', costo='5000', tipo=Mantenimiento.PREVENTIVO,
                             resultado=Mantenimiento.DISPONIBLE, estado=Mantenimiento.CONFIRMADO)
