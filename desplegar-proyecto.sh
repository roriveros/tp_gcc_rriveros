#! /bin/bash
# Variables utilizadas durante el script
git_user=
git_pass=

db_user=admin
db_pass=admin
db_name=dubium

env_name=prod_env

carpeta=prueba
camino=~/$carpeta
proyecto=$camino/proyecto-is2

# creacion de la carpeta y clonacion del proyecto

echo $camino

# Si la carpeta existe, la recreamos
if [ -d "$camino" ]; then
    echo "Entro"
    rm -rf $camino
fi
    mkdir $camino


cd $camino

git clone https://$git_user:$git_pass@gitlab.com/Lvmb/proyecto-is2.git

cd $proyecto

latesttag=$(git describe --tags)
echo checking out ${latesttag}
git checkout ${latesttag}


# creacion de la base de datos

#sudo -u postgres psql -tc "SELECT 1 FROM pg_database WHERE datname = 'my_db'" | grep -q 1 || sudo -u postgres psql -c "CREATE DATABASE my_db"

if [ "$(sudo -su postgres psql -tAc "SELECT 1 FROM pg_database WHERE datname='$db_name'" )" = '1' ]
then
    echo "Database already exists"
    sudo -u postgres psql -c "DROP DATABASE $db_name;"
fi

sudo -u postgres psql -c "CREATE DATABASE $db_name;"

if [ "$(sudo -su postgres psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='$db_user'" )" = '1' ]
then
    echo "User already exists"
    sudo -u postgres psql -c "DROP USER $db_user;"
fi

sudo -u postgres psql -c "CREATE USER $db_user WITH PASSWORD '$db_pass';"
sudo -u postgres psql -c "ALTER ROLE $db_user SET client_encoding TO 'utf8';"
sudo -u postgres psql -c "ALTER ROLE $db_user SET default_transaction_isolation TO 'read committed';"
sudo -u postgres psql -c "ALTER ROLE $db_user SET timezone TO 'UTC';"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE $db_name TO $db_user;"

cd ..


virtualenv -p python3 $env_name

source $env_name/bin/activate

#pip install django psycopg2 appdirs chartjs django-bootstrap-datepicker django-datetime-widget packaging pyparsing pytz six

cd $proyecto

pip install -r requirements.txt

python manage.py makemigrations modusuario

python manage.py makemigrations modrecurso


rm -rf modusuario/migrations/0001_initial.py

rm -rf modrecurso/migrations/0001_initial.py

python manage.py makemigrations modusuario

python manage.py makemigrations modmantenimiento

python manage.py makemigrations modpublico

python manage.py makemigrations modreserva

python manage.py migrate

python manage.py shell < datos.py

python manage.py collectstatic



#python manage.py runserver 0.0.0.0:8000

celery -A dubium worker -l info > ~/output.log 2>&1 &

text="<VirtualHost *:80>
	# The ServerName directive sets the request scheme, hostname and port that
	# the server uses to identify itself. This is used when creating
	# redirection URLs. In the context of virtual hosts, the ServerName
	# specifies what hostname must appear in the request's Host: header to
	# match this virtual host. For the default virtual host (this file) this
	# value is not decisive as it is used as a last resort host regardless.
	# However, you must set it for any further virtual host explicitly.
	#ServerName www.example.com

	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/html

	# Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
	# error, crit, alert, emerg.
	# It is also possible to configure the loglevel for particular
	# modules, e.g.
	#LogLevel info ssl:warn

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

	# For most configuration files from conf-available/, which are
	# enabled or disabled at a global level, it is possible to
	# include a line for only one particular virtual host. For example the
	# following line enables the CGI configuration for this host only
	# after it has been globally disabled with "a2disconf".
	#Include conf-available/serve-cgi-bin.conf

    Alias /static $proyecto/staticfiles
    <Directory $proyecto/staticfiles>
        Require all granted
    </Directory>

    <Directory $proyecto/dubium>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

    WSGIDaemonProcess dubium python-path=$proyecto python-home=$camino/$env_name
    WSGIProcessGroup dubium
    WSGIScriptAlias / $proyecto/dubium/wsgi.py

</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet"

echo "$text" | sudo tee /etc/apache2/sites-available/000-default.conf

#sudo echo "$text" > /etc/apache2/sites-available/000-default.conf

sudo service apache2 restart

firefox http://localhost/


