.. dubium documentation master file, created by
   sphinx-quickstart on Mon May  8 03:26:37 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dubium's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/modusuario/models
   modules/modusuario/views
   modules/modusuario/forms
   modules/modusuario/tests


   modules/modrecurso/models
   modules/modrecurso/views
   modules/modrecurso/forms
   modules/modrecurso/tests


   modules/modmantenimiento/models
   modules/modmantenimiento/views
   modules/modmantenimiento/forms
   modules/modmantenimiento/tests


   modules/modreserva/models
   modules/modreserva/views
   modules/modreserva/forms
   modules/modreserva/tests

   modules/modpublico/models
   modules/modpublico/views
   modules/modpublico/forms


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`