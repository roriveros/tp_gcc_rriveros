from django.apps import AppConfig


class ModmantenimientoConfig(AppConfig):
    name = 'modmantenimiento'
