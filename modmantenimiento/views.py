from django.shortcuts import render
from .models import Mantenimiento
from modrecurso.models import Recurso
from modreserva.models import Reserva
from .forms import FormCrearMantenimientoPreventivo, FormCrearMantenimientoCorrectivo, FormEditarMantenimientoCorrectivo, FormBuscarRecurso
from django.contrib import messages
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse_lazy
from datetime import date, datetime, time, timedelta
import datetime
from django.contrib.auth.decorators import login_required

@login_required
def crear_mantenimiento_preventivo(request):
    """
    Vista que sirve para crear mantenimiento del tipo preventivo donde se utiliza el FormCrearMantenimientoPreventivo, una vez llenado y validado, se abre el template de mantenimiento_creado
    """
    if request.method == 'POST':
        mantenimiento_form = FormCrearMantenimientoPreventivo(request.POST)
        if mantenimiento_form.is_valid():
            nuevo_mantenimiento = mantenimiento_form.save(commit=False)
            nuevo_mantenimiento.tipo = Mantenimiento.PREVENTIVO
            nuevo_mantenimiento.resultado = Mantenimiento.DISPONIBLE
            nuevo_mantenimiento.estado = Mantenimiento.CONFIRMADO
            nuevo_mantenimiento.save()
            #return redirect(reverse_lazy('modmantenimiento:mantenimiento_lista'))
            return render(request, 'modmantenimiento/mantenimiento_creado.html', {'form': mantenimiento_form})
    else:
        request.session['nombre_url'] = 'modmantenimiento:crear_mantenimiento_preventivo'
        if 'recurso_id' in request.session and request.session['recurso_id']:
            recurso_id = request.session['recurso_id']
            print('El id del recurso es:', recurso_id)
            request.session['recurso_id'] = None
            recurso = Recurso.objects.get(id=recurso_id)
            mantenimiento_form = FormCrearMantenimientoPreventivo(initial={'recurso': recurso, 'recurso_nombre': recurso.nombre})
        else:
            mantenimiento_form = FormCrearMantenimientoPreventivo()
    return render(request, 'modmantenimiento/crear_mantenimiento.html', {'form': mantenimiento_form})

@login_required
def crear_mantenimiento_correctivo(request):
    """
    Vista que sirve para crear mantenimiento del tipo correctivo donde se utiliza el FormCrearMantenimientoCorrectivo, una vez llenado y validado, se abre el template de mantenimiento_creado
    """
    if request.method == 'POST':
        mantenimiento_form = FormCrearMantenimientoCorrectivo(request.POST)
        if mantenimiento_form.is_valid():
            nuevo_mantenimiento = mantenimiento_form.save(commit=False)
            nuevo_mantenimiento.tipo = Mantenimiento.CORRECTIVO
            nuevo_mantenimiento.estado = Mantenimiento.CONFIRMADO
            #recurso = nuevo_mantenimiento.recurso
            #recurso.estado = Recurso.MANTENIMIENTO
            #recurso.save()
            nuevo_mantenimiento.save()
            #return redirect(reverse_lazy('modm antenimiento:mantenimiento_lista'))
            return render(request, 'modmantenimiento/mantenimiento_creado.html', {'form': mantenimiento_form})
    else:
        request.session['nombre_url'] = 'modmantenimiento:crear_mantenimiento_correctivo'
        if 'recurso_id' in request.session and request.session['recurso_id']:
            recurso_id = request.session['recurso_id']
            print('El id del recurso es:', recurso_id)
            request.session['recurso_id'] = None
            recurso = Recurso.objects.get(id=recurso_id)
            mantenimiento_form = FormCrearMantenimientoCorrectivo(
                initial={'recurso': recurso, 'recurso_nombre': recurso.nombre})
        else:
            mantenimiento_form = FormCrearMantenimientoCorrectivo()
    return render(request, 'modmantenimiento/crear_mantenimiento.html', {'form': mantenimiento_form})


@login_required
def mantenimiento_list(request):
    """
    Vista que sirve para listar todos los mantenimientos, incluye un paginator de manera a que se presente de manera mas organizada los mantenimientos 
    """
    if request.user.has_perm('modmantenimiento.list_all_mantenimiento'):
        mantenimiento_list = Mantenimiento.objects.all().order_by('-id')
    else:
        mantenimiento_list = Mantenimiento.objects.filter(recurso__area = request.user.profile.area, recurso__tipo_recurso__profile = request.user.profile).order_by('-id')

    page = request.GET.get('page', 1)
    paginator = Paginator(mantenimiento_list, 15)
    try:
        mantenimientos = paginator.page(page)
    except PageNotAnInteger:
        mantenimientos = paginator.page(1)
    except EmptyPage:
        mantenimientos = paginator.page(paginator.num_pages)
    return render(request, 'modmantenimiento/mantenimiento_lista.html', {'mantenimientos': mantenimientos})

@login_required
def eliminar_mantenimiento(request, parameter):
    """
    Vista que sirve para eliminar mantenimientos, el cual elimina completamente si todavia no ha llegado la fecha y cancela si es que la fecha ya ha llegado
    """

    mantenimiento = Mantenimiento.objects.get(pk=parameter)
    fecha_hoy = timezone.make_aware(datetime.datetime.now())
    print(fecha_hoy)
    print(mantenimiento.fecha_hora_inicio)
    if(mantenimiento.fecha_hora_inicio > fecha_hoy):
        mantenimiento.delete()
        return render(request, 'modmantenimiento/eliminar_mantenimiento.html')

    elif(mantenimiento.estado != Mantenimiento.CANCELADO):

        if mantenimiento.tipo == Mantenimiento.CORRECTIVO:
            mantenimiento.recurso.estado = Recurso.DESCOMPUESTO

        elif mantenimiento.tipo == Mantenimiento.PREVENTIVO:
            mantenimiento.recurso.estado = Recurso.DISPONIBLE

        mantenimiento.estado = Mantenimiento.CANCELADO
        mantenimiento.recurso.save()
        mantenimiento.save()

    return redirect(reverse_lazy('modmantenimiento:mantenimiento_lista'))

@login_required
def modificar_mantenimiento(request, parameter):
    """
    Vista utilizada para modificar un recurso con el modelo `modrecurso.Mantenimiento` en donde se ingresan los datos del mantenimiento y los recursos afectados se pasan a los estados correspondientes
    """
    mantenimiento = Mantenimiento.objects.get(pk=parameter)
    mantenimiento.fecha_hora_fin = datetime.datetime.now().strftime('%d/%m/%Y %H:%M:%S')
    if request.method == 'POST':
        form = FormEditarMantenimientoCorrectivo(instance=mantenimiento, data=request.POST)
        if form.is_valid():
            mantenimiento_editado = form.save(commit=False)
            recurso = mantenimiento_editado.recurso
            if mantenimiento_editado.resultado == Mantenimiento.DISPONIBLE:
                recurso.estado = Recurso.DISPONIBLE
            elif mantenimiento_editado.resultado == Mantenimiento.DESCOMPUESTO:
                recurso.estado = Recurso.DESCOMPUESTO
            elif mantenimiento_editado.resultado == Mantenimiento.INUTILIZABLE:
                recurso.estado = Recurso.INUTILIZABLE
            mantenimiento.save()
            recurso.save()
            return render(request, 'modmantenimiento/mantenimiento_modificado.html')
        else:
            messages.error(request, 'Error al actualizar Mantenimiento')
    else:
        form = FormEditarMantenimientoCorrectivo(instance=mantenimiento)
    return render(request, 'modmantenimiento/modificar_mantenimiento.html', {'form': form})

@login_required
def buscar_recurso_lista(request):

    recurso_list = None
    pagrecurso_list = None
    nombre_url = request.session['nombre_url']
    if request.method == 'GET':
        valores = {  # aca se van poniendo los valores para filtrar el query
        }
        arguementos = {}
        form = FormBuscarRecurso(request.GET)
        if form.is_valid():
            cd = form.cleaned_data
            valores['nombre__icontains'] = cd.get('nombre')
            valores['area'] = cd.get('area')
            valores['tipo_recurso'] = cd.get('tipo_recurso')
            if nombre_url == 'modmantenimiento:crear_mantenimiento_preventivo':
                valores['estado'] = Recurso.DISPONIBLE
            elif nombre_url == 'modmantenimiento:crear_mantenimiento_correctivo':
                valores['estado'] = Recurso.DESCOMPUESTO

        # Este for se utiliza para determinar si una "clave" tiene valor o no, si no tiene, no se incluye en el filtro
        for k, v in valores.items():
            if v:
                arguementos[k] = v

        pagrecurso_list = Recurso.objects.filter(**arguementos).order_by('tipo_recurso__nombre', 'nombre')

        page = request.GET.get('page', 1)
        paginator = Paginator(pagrecurso_list, 15)

        try:
            recurso_list = paginator.page(page)
        except PageNotAnInteger:
            recurso_list = paginator.page(1)
        except EmptyPage:
            recurso_list = paginator.page(paginator.num_pages)

        return render(request, 'modmantenimiento/buscar_recurso_lista.html', {'form': form, 'recurso_list': recurso_list, 'nombre_url': nombre_url})

    if request.method == 'POST':
        recurso_id = request.POST.get('recurso_id')
        request.session['recurso_id'] = recurso_id
        # Returns None if user came from another website
        print(nombre_url)
        print(recurso_id)
        return redirect(reverse_lazy(nombre_url))