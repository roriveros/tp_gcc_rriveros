# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
from modrecurso.models import Recurso

@shared_task
def set_mantenimiento_as_active(recurso_id):
    """
    This celery task sets the 'is_active' flag of the race object
    to False in the database after the race end time has elapsed.
    """
    print("Entro a celery")
    print(recurso_id)
    recurso = Recurso.objects.get(pk=recurso_id)
    recurso.estado = Recurso.MANTENIMIENTO # set the race as not active
    print(recurso.estado)
    recurso.save() # save the race object

@shared_task
def set_mantenimiento_as_inactive(recurso_id):
    """
    This celery task sets the 'is_active' flag of the race object
    to False in the database after the race end time has elapsed.
    """
    print("Entro a celery")
    print(recurso_id)
    recurso = Recurso.objects.get(pk=recurso_id)
    recurso.estado = Recurso.DISPONIBLE # set the race as not active
    print(recurso.estado)
    recurso.save() # save the race object

@shared_task
def set_recurso_as_active(recurso_id):
    """
    This celery task sets the 'is_active' flag of the race object
    to False in the database after the race end time has elapsed.
    """
    print("Entro a celery")
    print(recurso_id)
    recurso = Recurso.objects.get(pk=recurso_id)
    recurso.estado = Recurso.RESERVADO # set the race as not active
    print(recurso.estado)
    recurso.save() # save the race object
