from django.db import models
from modrecurso.models import TipoRecurso,Recurso
from .tasks import set_mantenimiento_as_active, set_mantenimiento_as_inactive

class Mantenimiento(models.Model):
    """
        Modelo utilizado para la creacion de mantenimientos de recursos donde se solicitan los datos necesarios para un mantenimiento
    """

    recurso = models.ForeignKey('modrecurso.Recurso', on_delete=models.CASCADE)

    fecha_hora_inicio = models.DateTimeField()
    fecha_hora_fin = models.DateTimeField(null=True, blank=True)
    motivo = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=100)
    costo= models.FloatField(max_length=100, null=True, blank=True)
    PREVENTIVO='PREVENTIVO'
    CORRECTIVO='CORRECTIVO'
    TIPO_CHOICES = (
        (PREVENTIVO, 'Preventivo'),
        (CORRECTIVO, 'Correctivo'),
    )
    tipo = models.CharField(
        max_length=10,
        choices=TIPO_CHOICES
    )
    CONFIRMADO = 'CONFIRMADO'
    CANCELADO = 'CANCELADO'
    ESTADO_CHOICES = (
        (CONFIRMADO, 'Confirmado'),
        (CANCELADO, 'Cancelado'),
    )
    estado = models.CharField(
        max_length=10,
        choices=ESTADO_CHOICES,
        blank=True,
        null=True
    )
    DISPONIBLE = 'DISPONIBLE'
    DESCOMPUESTO = 'DESCOMPUESTO'
    INUTILIZABLE = 'INUTILIZABLE'
    RESULTADO_CHOICES = (
        (DISPONIBLE, 'Disponible'),
        (DESCOMPUESTO, 'Descompuesto'),
        (INUTILIZABLE, 'Inutilizable'),
    )
    resultado = models.CharField(
        max_length=12,
        choices=RESULTADO_CHOICES
    )


    class Meta:
        permissions = (
            ("list_all_mantenimiento", "Can list all mantenimiento"),
            ("list_mantenimiento", "Can list a certain mantenimiento"),
        )

    def __str__(self):
        return self.tipo

    def save(self, *args, **kwargs):

        create_task = False  # variable to know if celery task is to be created
        create_task2 = False
        if self.pk is None:  # Check if instance has 'pk' attribute set
            # Celery Task is to created in case of 'INSERT'
            create_task = True  # set the variable
            if self.fecha_hora_fin:
                create_task2 = True

        super(Mantenimiento, self).save(*args, **kwargs)  # Call the Django's "real" save() method.

        if create_task:  # check if task is to be created
            # pass the current instance as 'args' and call the task with 'eta' argument
            # to execute after the race `end_time`
            set_mantenimiento_as_active.apply_async(args=[self.recurso.id],
                                             eta=self.fecha_hora_inicio)  # task will be executed after 'race_end_time'

        if create_task2:
            set_mantenimiento_as_inactive.apply_async(args=[self.recurso.id],
                                                    eta=self.fecha_hora_fin)
