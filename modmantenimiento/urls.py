from django.conf.urls import url
from . import views

app_name = 'modmantenimiento'

urlpatterns = [
    # crear mantenimiento preventivo
    url(r'^crear_preventivo/$', views.crear_mantenimiento_preventivo, name='crear_mantenimiento_preventivo'),
    # crear mantenimiento correctivo
    url(r'^crear_correctivo/$', views.crear_mantenimiento_correctivo, name='crear_mantenimiento_correctivo'),
    # modificar mantenimiento
    url(r'^modificar-(?P<parameter>\d+)/', views.modificar_mantenimiento, name='modificar_mantenimiento'),
    # lista de mantenimientos
    url(r'^listar/$', views.mantenimiento_list, name='mantenimiento_lista'),
    # eliminar mantenimientos
    url(r'^eliminar-(?P<parameter>[\w-]+).html', views.eliminar_mantenimiento, name='eliminar_mantenimiento'),

    url(r'^buscar_recurso/$', views.buscar_recurso_lista, name='buscar_recurso_lista'),
]
