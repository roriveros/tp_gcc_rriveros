from django import forms
from .models import Mantenimiento
from modrecurso.models import Recurso, TipoRecurso
from modusuario.models import Area
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget
from datetime import datetime, date, time, timedelta, timezone
from  django.utils import timezone
import pytz, datetime

class FormCrearMantenimientoPreventivo(forms.ModelForm):
    """
         Formulario que se utiliza para la visualizacion de mantenimientos preventivos, para el ingreso de fechas_horas se muestran widgets.
     """

    def __init__(self, *args, **kwargs):
        super(FormCrearMantenimientoPreventivo, self).__init__(*args, **kwargs)
        #self.fields['recurso'] = forms.ModelChoiceField(required=True, queryset=Recurso.objects.filter(estado=Recurso.DISPONIBLE))
        self.fields['recurso'].help_text = 'Los recursos que estan en estado ' + Recurso.DISPONIBLE

    recurso_nombre = forms.CharField(label="Recurso", widget=forms.TextInput(attrs={'readonly':'readonly'}))
    costo = forms.FloatField(min_value=0, required=True)
    fecha_hora_inicio = forms.DateTimeField(label="Fecha de Inicio", input_formats=['%d/%m/%Y %H:%M:%S', '%d/%m/%Y %H:%M'], widget = DateTimeWidget(usel10n=True, attrs={'id': "fecha_hora_inicio", 'class': 'input[type=date].form-control'}, bootstrap_version=2))
    fecha_hora_fin = forms.DateTimeField(label="Fecha de Fin", required=True, input_formats=['%d/%m/%Y %H:%M:%S', '%d/%m/%Y %H:%M'], widget = DateTimeWidget(usel10n=True, attrs={'id': "fecha_hora_fin", 'class': 'input[type=date].form-control'}, bootstrap_version=2))

    class Meta:
        model = Mantenimiento
        fields = ('recurso_nombre', 'recurso','fecha_hora_inicio', 'fecha_hora_fin', 'motivo', 'descripcion', 'costo')
        widgets = {'recurso': forms.HiddenInput(), 'descripcion': forms.Textarea}

    def clean_fecha_hora_fin(self):
        print(self.cleaned_data)
        fecha_ini = self.cleaned_data['fecha_hora_inicio']
        fecha_fin = self.cleaned_data['fecha_hora_fin']
        fecha_semana =  timezone.make_aware(datetime.datetime.now() + datetime.timedelta(days=7)) # esto funcionaba antes?
        tiempo_inicial = datetime.time(7, 15, 0, 0)
        tiempo_final = datetime.time(22, 15, 0, 0)

        #tiempo_final =  + timedelta(hours=22, minutes=15)
        if fecha_ini < fecha_semana:
            raise forms.ValidationError('La fecha inicial debe ser almenos 1 semana despues de la fecha actual')
        elif fecha_ini > fecha_fin:
            raise forms.ValidationError('La fecha inicial es mayor que la fecha final')
        elif fecha_ini.time() < tiempo_inicial or fecha_ini.time() > tiempo_final or fecha_fin.time() > tiempo_final:     # para que solo se puede crear man tenimiento en el rango de hora de 7:15 a 22:15
            raise forms.ValidationError('La hora debe ser entre 7:15 a 22:15')
        return fecha_fin

class FormCrearMantenimientoCorrectivo(forms.ModelForm):
    """
         Formulario que se utiliza para la visualizacion de mantenimientos correctivos, se tiene mensajes de ayuda y restricciones.
     """
    def __init__(self, *args, **kwargs):
        super(FormCrearMantenimientoCorrectivo, self).__init__(*args, **kwargs)
        #self.fields['recurso'] = forms.ModelChoiceField(required=True, queryset=Recurso.objects.filter(estado=Recurso.DESCOMPUESTO))
        self.fields['recurso'].help_text = 'Los recursos que estan en estado ' + Recurso.DESCOMPUESTO

    recurso_nombre = forms.CharField(label="Recurso", widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    fecha_hora_inicio = forms.DateTimeField(label="Fecha de Inicio", input_formats=['%d/%m/%Y %H:%M:%S', '%d/%m/%Y %H:%M'], widget=DateTimeWidget(usel10n=True, attrs={'id': "fecha_hora_inicio", 'class': 'input[type=date].form-control'}, bootstrap_version=2))

    class Meta:
        model = Mantenimiento
        fields = ('recurso_nombre', 'recurso','fecha_hora_inicio', 'motivo')
        widgets = {'recurso': forms.HiddenInput()}

    def clean_fecha_hora_inicio(self):
        fecha_ini = self.cleaned_data['fecha_hora_inicio']
        fecha_hoy = timezone.make_aware(datetime.datetime.now())
        if fecha_ini <= fecha_hoy:
            raise forms.ValidationError('La fecha inicial minima debe ser a partir de la fecha actual')
        return fecha_ini

class FormEditarMantenimientoCorrectivo(forms.ModelForm):
    """
             Formulario que se utiliza para la modificacion de mantenimientos, con ayudas y widgets.
    """

    costo = forms.FloatField(min_value=0, required=True)
    fecha_hora_fin = forms.DateTimeField(label="Fecha de Fin", required=True, input_formats=['%d/%m/%Y %H:%M:%S', '%d/%m/%Y %H:%M'], widget = forms.DateTimeInput(attrs={'readonly': True}))

    class Meta:
        model = Mantenimiento
        fields = ('fecha_hora_fin', 'descripcion', 'costo', 'resultado')

        widgets = {'descripcion': forms.Textarea}

        help_texts = {
            'descripcion': 'Cosas que se le realizaron al recurso',
            'resultado': 'Si el recurso sigue descompuesto o puede ser utilizado'
        }

class FormBuscarRecurso(forms.Form):
    """
        Formulario utilizado para buscar un recurso

    """

    def __init__(self, *args, **kwargs):
        super(FormBuscarRecurso, self).__init__(*args, **kwargs)
        self.fields['nombre'] = forms.CharField(required=False)
        self.fields['area'] = forms.ModelChoiceField(required=False, queryset=Area.objects.all())
        self.fields['tipo_recurso'] = forms.ModelChoiceField(required=False, queryset=TipoRecurso.objects.all())