from django.apps import AppConfig


class ModreservaConfig(AppConfig):
    name = 'modreserva'
