from django import forms
from .models import Recurso, TipoRecurso,Historial
from .models import Reserva, Recurso
from datetimewidget.widgets import DateWidget, TimeWidget,DateTimeWidget
from datetime import date, time, timedelta
from modusuario.models import Area
from django.forms.widgets import HiddenInput
from modusuario.email_config import fecha_reserva
import datetime
from django.contrib.auth.models import User

class FormReserva(forms.ModelForm):
    """
        Formulario que se utiliza para solicitar los datos necesarios para una reserva directa
         
    """


    def clean_hora_final(self):
        cd = self.cleaned_data
        if cd['hora_inicio'] > cd['hora_final'] or cd['hora_inicio'] < datetime.datetime.now().time():
            raise forms.ValidationError('La hora inicial debe ser mayor a la actual y mayor a la hora final')
        return cd['hora_final']





    #fecha_reserva = forms.DateField(label="Fecha de Reserva", input_formats=['%d/%m/%Y'], widget=DateWidget(usel10n=True,attrs={'id': "fecha_reserva",'class': 'input[type=date].form-control'},bootstrap_version=2))
    hora_inicio = forms.TimeField(label="Hora de Inicio", widget=TimeWidget(usel10n=True,attrs={'id': "hora_inicio",'class': 'input[type=time].form-control'},bootstrap_version=2))
    hora_final = forms.TimeField(label="Hora de Fin", widget=TimeWidget(usel10n=True,attrs={'id': "hora_final",'class': 'input[type=time].form-control'},bootstrap_version=2))


    class Meta:
        model = Reserva
        fields = ('recurso','tipo_recurso','hora_inicio', 'hora_final')

    def __init__(self, *args, **kwargs):
        super(FormReserva, self).__init__(*args, **kwargs)
        self.fields['recurso'].widget = HiddenInput()
        #self.fields['recurso'].widget.attrs['readonly'] = True



class FormReservaAnt(forms.ModelForm):
    """
         Formulario que se utiliza para solicitar los datos necesarios para una reserva anticipada

    """

    # def __init__(self, *args, **kwargs):
    #     super(FormReservaAnt, self).__init__(*args, **kwargs)
    #     self.fields['recurso'].queryset = Recurso.objects.filter(estado='DISPONIBLE')


    fecha_reserva = forms.DateField(label="Fecha de Reserva", input_formats=['%d/%m/%Y'], widget=DateWidget(usel10n=True,attrs={'id': "fecha_reserva",'class': 'input[type=date].form-control'},bootstrap_version=2))
    hora_inicio = forms.TimeField(label="Hora de Inicio", widget=TimeWidget(usel10n=True,attrs={'id': "hora_inicio",'class': 'input[type=time].form-control'},bootstrap_version=2))
    hora_final = forms.TimeField(label="Hora de Fin", widget=TimeWidget(usel10n=True,attrs={'id': "hora_final",'class': 'input[type=time].form-control'},bootstrap_version=2))


    class Meta:
        model = Reserva
        fields = ('recurso',"tipo_recurso","fecha_reserva", 'hora_inicio', 'hora_final','dias')

    def __init__(self, *args, **kwargs):
        super(FormReservaAnt, self).__init__(*args, **kwargs)
        self.fields['recurso'].widget = HiddenInput()
        #self.fields['recurso'].widget.attrs['readonly'] = True
        #Atodo hacer que no desaparezca la muestra del nombre del recurso cuando se ingresan mal los datos


    def clean_hora_final(self):
        cd = self.cleaned_data
        if cd['hora_inicio'] > cd['hora_final']:
            raise forms.ValidationError('La hora inicial es mayor que la hora final')
        return cd['hora_final']

    def clean_fecha_reserva(self):
        cd = self.cleaned_data
        today = date.today() +timedelta(int(fecha_reserva) - 1)
        if cd['fecha_reserva'] <= today:
            raise forms.ValidationError('Debe elejir una fecha mayor')
        return cd['fecha_reserva']

    def clean_dias(self):
        cd = self.cleaned_data
        if cd['dias'] < 1:
            raise forms.ValidationError('La cantidad a dias a ser reservados debe ser mayor a 0')
        return cd['dias']
    # def clean_tipo_recurso(self):
    #     cd = self.cleaned_data
    #     if cd['tipo_recurso'] == None:
    #         raise forms.ValidationError('Debe insertar al menos un tipo de recurso a ser reservado')
    #     return cd['tipo_recurso']


class FormModificar(forms.ModelForm):
    """
    Formulario utilizado para solicitar los datos a ser aletrados para la modificcion de la reserva
    """
    class Meta:
        model = Reserva
        fields = ('usuario','tipo_recurso', 'recurso','estado')

    # def __init__(self, *args, **kwargs):
    #     super(FormModificar, self).__init__(*args, **kwargs)
    #     self.fields['recurso'].queryset = Recurso.objects.filter(estado='DISPONIBLE')

class FormHistorial(forms.ModelForm):
    """
    Formulario utilizado para solicitar los datos necesarios para un reporte
    """
    descripcion = forms.CharField(widget=forms.Textarea)

    # def __init__(self, *args, **kwargs):
    #     super(FormHistorial, self).__init__(*args, **kwargs)  # Call to ModelForm constructor
    #     self.fields['descripcion'].widget.attrs['style'] = 'width:800px; height:80px;'
    #     #self.fields['usuario'] = forms.ModelChoiceField(required=False, queryset=User.objects.all(),label='Usuario Devolucion')

    class Meta:
        model = Historial
        fields = ('descripcion','estado','usuario_dev')
        help_texts = {
            'descripcion': 'Exprese como ha recibido el recurso.'
        }
    def __init__(self, *args, **kwargs):
        super(FormHistorial, self).__init__(*args, **kwargs)  # Call to ModelForm constructor
        self.fields['descripcion'].widget.attrs['style'] = 'width:800px; height:80px;'
        self.fields['usuario_dev'].label="Devuelto por"

class FormFiltros(forms.Form):

    fecha_desde = forms.DateField(label="Fecha Desde", required=False, input_formats=['%d/%m/%Y'], widget=DateWidget(usel10n=True,attrs={'id': "fecha_desde",'class': 'input[type=date].form-control'},bootstrap_version=2))
    fecha_hasta = forms.DateField(label="Fecha Hasta", required=False, input_formats=['%d/%m/%Y'], widget=DateWidget(usel10n=True,attrs={'id': "fecha_hasta",'class': 'input[type=date].form-control'},bootstrap_version=2))
    usuario = forms.CharField(required=False)
    recurso = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        super(FormFiltros, self).__init__(*args, **kwargs)
        ESTADO_CHOICES = (('', 'Todos'),) + Reserva.ESTADO_CHOICES
        TIPO_CHOICES = (('', 'Todos'),) + Reserva.TIPO_CHOICES

        self.fields['estado'] = forms.ChoiceField(required=False, choices=ESTADO_CHOICES)
        self.fields['tipo'] = forms.ChoiceField(required=False, choices=TIPO_CHOICES)
        self.fields['tipo_recurso'] = forms.ModelChoiceField(required=False, queryset=TipoRecurso.objects.all(), empty_label='Todos')

class BuscarRecursoForm(forms.Form):
    """
        Formulario utilizado para buscar un recurso

    """
    hora_inicio = forms.TimeField(label="Hora de Inicio", widget=TimeWidget(usel10n=True, attrs={'id': "hora_inicio",'class': 'input[type=time].form-control'},bootstrap_version=2))
    hora_final = forms.TimeField(label="Hora de Fin", widget=TimeWidget(usel10n=True, attrs={'id': "hora_final",'class': 'input[type=time].form-control'},bootstrap_version=2))

    def __init__(self, *args, request_data=None, **kwargs):
    #def __init__(self, nombre,tipo_recurso,hora_inicio,hora_final, request_data=None, **kwargs):
        super(BuscarRecursoForm, self).__init__(*args, **kwargs)
        #super(BuscarRecursoForm, self).__init__(nombre,tipo_recurso,hora_inicio,hora_final, **kwargs)
        self.fields['nombre'] = forms.CharField(required=False)
        self.fields['area'] = forms.ModelChoiceField(required=False, queryset=Area.objects.all())
        self.fields['tipo_recurso'] = forms.ModelChoiceField(required=False, queryset=TipoRecurso.objects.all())
        #self.fields['hora_inicio'] = forms.TimeField(required=False,widget=forms.TimeInput(format='%H:%M:%S'))
        self.fields['hora_inicio'] = forms.TimeField(required=False, widget=TimeWidget(usel10n=True,attrs={'id': "hora_inicio",'class': 'input[type=time].form-control'},bootstrap_version=2))
        #self.fields['hora_final'] = forms.TimeField(required=False)
        self.fields['hora_final'] = forms.TimeField(required=False, widget=TimeWidget(usel10n=True,attrs={'id': "hora_final",'class': 'input[type=time].form-control'},bootstrap_version=2))
        #self.fields['fecha'] = forms.DateField(required=False, initial=date.today())
        self.fields['fecha'] = forms.DateField(required=False, widget=DateWidget(usel10n=True,attrs={'id': "hora_inicio",'class': 'input[type=time].form-control'},bootstrap_version=2),initial=date.today())

class FormRecuperar(forms.ModelForm):
    """
    Formulario utilizado para solicitar los datos necesarios para un reporte
    """
    #descripcion = forms.CharField(widget=forms.Textarea)

    # def __init__(self, *args, **kwargs):
    #     super(FormHistorial, self).__init__(*args, **kwargs)  # Call to ModelForm constructor
    #     self.fields['descripcion'].widget.attrs['style'] = 'width:800px; height:80px;'
    #     #self.fields['usuario'] = forms.ModelChoiceField(required=False, queryset=User.objects.all(),label='Usuario Devolucion')
    fecha_hora_entrega = forms.DateTimeField(label="Fecha de Entrega", input_formats=['%d/%m/%Y %H:%M:%S', '%d/%m/%Y %H:%M'],
                        widget=DateTimeWidget(usel10n=True, attrs={'id': "fecha_hora_entrega",
                                                                   'class': 'input[type=date].form-control'}, bootstrap_version=2))
    fecha_hora_devolucion = forms.DateTimeField(label="Fecha de Devolucion", input_formats=['%d/%m/%Y %H:%M:%S', '%d/%m/%Y %H:%M'],
                        widget=DateTimeWidget(usel10n=True, attrs={'id': "fecha_hora_devolucion",
                                                                   'class': 'input[type=date].form-control'}, bootstrap_version=2))
    class Meta:
        model = Reserva
        fields = ('fecha_hora_entrega','fecha_hora_devolucion')
    # def __init__(self, *args, **kwargs):
    #     super(FormRecuperar, self).__init__(*args, **kwargs)  # Call to ModelForm constructor
    #     self.fields['descripcion'].widget.attrs['style'] = 'width:800px; height:80px;'
    #     self.fields['usuario_dev'].label="Devuelto por"

class FormReservaPdf(forms.Form):

    fecha_desde = forms.DateField(label="Fecha Desde", required=False, input_formats=['%d/%m/%Y'], widget=DateWidget(usel10n=True,attrs={'id': "fecha_desde",'class': 'input[type=date].form-control'},bootstrap_version=2))
    fecha_hasta = forms.DateField(label="Fecha Hasta", required=False, input_formats=['%d/%m/%Y'], widget=DateWidget(usel10n=True,attrs={'id': "fecha_hasta",'class': 'input[type=date].form-control'},bootstrap_version=2))

    def __init__(self, *args, **kwargs):
        super(FormReservaPdf, self).__init__(*args, **kwargs)
        ESTADO_CHOICES = (('', 'Todos'),) + Reserva.ESTADO_CHOICES
        TIPO_CHOICES = (('', 'Todos'),) + Reserva.TIPO_CHOICES


        self.fields['tipo'] = forms.ChoiceField(required=False, choices=TIPO_CHOICES)
        self.fields['estado'] = forms.ChoiceField(required=False, choices=ESTADO_CHOICES)
        self.fields['tipo_recurso'] = forms.ModelChoiceField(required=False, queryset=TipoRecurso.objects.all(),empty_label='Todos')
