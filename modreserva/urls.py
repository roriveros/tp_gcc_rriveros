from django.conf.urls import url
from . import views

app_name = 'modreserva'

urlpatterns = [
    #url(r'^elegir/$', views.elegir_reserva, name='elegir_reserva'),
    # crear reserva
    url(r'^crear/$', views.crear_reserva, name='crear_reserva'),
    # lista de mantenimientos
    url(r'^listar/$', views.reserva_list, name='reserva_lista'),
    url(r'^listar/reporte_reserva_pdf/$',views.ReporteReservasPDF.as_view(), name='reporte_reserva_pdf'),
    # eliminar mantenimientos
    url(r'^eliminar-(?P<parameter>[\w-]+).html', views.eliminar_reserva, name='eliminar_reserva'),
    # url(r'^mantenimiento/$', views.mantenimiento_list),
    # crear reserva anticipada
    url(r'^crearA/$', views.crear_reserva_ant, name='crear_reserva_ant'),
    # resolver reservas anticipadas
    url(r'^resolver/$', views.reserva_list_resolver, name='reserva_list_resolver'),
    # entrega-devolucion de recursos de reservas
    url(r'^entregar-(?P<parameter>[\w-]+).html', views.entrega_recurso, name='entrega_recurso'),
    #cancelar reservas
    url(r'^cancelar-(?P<parameter>[\w-]+).html', views.cancelar_reserva, name='cancelar_reserva'),
    #vista previa de la resolucion de reservas
    #url(r'^preview/$', views.vista_previa, name='vista_previa'),
    url(r'^modificar-(?P<parameter>[\w-]+).html', views.modificar_reserva, name='modificar_reserva'),
    # crear reserva
    url(r'^crearH/$', views.crear_historial, name='crear_historial'),
    #buscar recurso
    url(r'^buscar_recurso_list/$', views.buscar_recurso_lista, name='buscar_recurso_list'),
    # editar datos de reserva
    url(r'^recuperar-(?P<parameter>[\w-]+).html', views.recuperar, name='recuperar'),
]
