from django.db import models
from django.contrib.admin.widgets import AdminDateWidget
from modusuario.models import Profile
from django.contrib.auth.models import User
from modrecurso.models import TipoRecurso,Recurso
from django.utils import timezone
from django.conf import settings
from datetime import datetime,date,time,timedelta
from modmantenimiento.tasks import set_recurso_as_active

class Reserva(models.Model):
    """
        Modelo utilizado para la creacion de reserva de recursos.
    """
    usuario = models.ForeignKey(settings.AUTH_USER_MODEL,blank=True)
    fecha_reserva = models.DateField(blank=True, default=date.today)
    dias=models.IntegerField(blank=True,default=1)
    tipo_recurso = models.ForeignKey('modrecurso.TipoRecurso', null=False)
    recurso = models.ForeignKey('modrecurso.Recurso',blank=True,null=True)
    hora_inicio=models.TimeField(blank=True, null=True)
    hora_final=models.TimeField(blank=True,default='00:00')
    DIRECTA = 'DIRECTA'
    ANTICIPADA = 'ANTICIPADA'
    TIPO_CHOICES = (
        (DIRECTA, 'Directa'),
        (ANTICIPADA, 'Anticipada'),
    )
    tipo = models.CharField(
             max_length=12,
             choices=TIPO_CHOICES,
             blank=True,
             null=True,
         )
    #descripcion = models.CharField(max_length=100, null=True, blank=True)
    ACEPTADA = 'ACEPTADA'
    RECHAZADA = 'RECHAZADA'
    CANCELADA = 'CANCELADA'
    FINALIZADA = 'FINALIZADA'
    EN_ESPERA = 'EN ESPERA'
    EN_CURSO = 'EN CURSO'
    ESTADO_CHOICES = (
        (ACEPTADA, 'Aceptada'),
        (RECHAZADA, 'Rechazada'),
        (CANCELADA, 'Cancelada'),
        (FINALIZADA, 'Finalizada'),
        (EN_ESPERA, 'En Espera'),
        (EN_CURSO, 'En Curso')
    )
    estado = models.CharField(max_length=20, choices=ESTADO_CHOICES, blank=True,null=True)
    fecha_hora_entrega=models.DateTimeField(blank=True,null=True)
    fecha_hora_devolucion = models.DateTimeField(blank=True, null=True)
    fecha=models.DateTimeField(auto_now_add=True, blank=False)
    class Meta:
        permissions = (
            ("list_all_reserva", "Can list all reserva"),
            ("list_reserva", "Can list certain reserva"),
            ("entrega_recurso", "Can manage recurso"),
            ("cancelar_reserva", "Can cancel reserva"),
        )

    def __str__(self):
        return str(self.id)
        #return 'Reserva del recurso {}'.format(self.recurso)
    def save(self, *args, **kwargs):

        create_task = False  # variable to know if celery task is to be created
        if self.pk is None:  # Check if instance has 'pk' attribute set
            # Celery Task is to created in case of 'INSERT'
            create_task = True  # set the variable

        super(Reserva, self).save(*args, **kwargs)  # Call the Django's "real" save() method.
        if create_task and self.recurso!=None:  # check if task is to be created
            # pass the current instance as 'args' and call the task with 'eta' argument
            # to execute after the race `end_time`

            #print(datetime.combine(self.fecha_reserva,self.hora_inicio))
            print(self.fecha_reserva)
            print(self.hora_inicio)
            print(self.fecha)

            set_recurso_as_active.apply_async(args=[self.recurso.id],
                                             eta=(datetime.combine(self.fecha_reserva,self.hora_inicio))+timedelta(hours=4))  # task will be executed after 'race_end_time'

class Historial(models.Model):
     usuario=models.ForeignKey(settings.AUTH_USER_MODEL,null=True,blank=True, related_name='usuario')
     usuario_dev=models.ForeignKey(settings.AUTH_USER_MODEL,null=True,blank=True, related_name='usuario_dev')
     recurso = models.ForeignKey('modrecurso.Recurso',null=True,blank=True)
     reserva=models.ForeignKey('modreserva.Reserva',null=True,blank=True)
     descripcion=models.CharField(max_length=100,null=True,blank=True,default="Se presento el recurso tal y cual como se retiro")
     DISPONIBLE = 'DISPONIBLE'
     DESCOMPUESTO = 'DESCOMPUESTO'
     ESTADO_CHOICES = (
         (DISPONIBLE, 'Disponible'),
         (DESCOMPUESTO, 'Descompuesto'),
     )
     estado = models.CharField(max_length=16, choices=ESTADO_CHOICES, blank=True, default='DISPONIBLE')

     def __str__(self):
         return self.descripcion
