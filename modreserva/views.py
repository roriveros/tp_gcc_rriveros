from django.shortcuts import render
from .models import Reserva,TipoRecurso
from django.contrib.auth.models import User
from .forms import FormReserva,FormReservaAnt,FormModificar,FormHistorial, FormFiltros,BuscarRecursoForm,FormRecuperar, FormReservaPdf
from modusuario.models import Profile,Area
from modrecurso.models import Recurso,TipoRecurso
from datetime import date, datetime, time, timedelta
import pytz
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse_lazy
from django.core.mail import send_mail
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from modusuario.email_config import fecha_reserva,tiempo_antes,tiempo_despues
from django.contrib.auth.decorators import login_required
from modmantenimiento.tasks import set_recurso_as_active

from django.conf import settings
from io import BytesIO
from reportlab.pdfgen import canvas
from django.views.generic import View
from django.http import HttpResponse
from django.views.generic import ListView
from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib import colors
from reportlab.platypus import Table
from reportlab.lib.units import cm, inch
from datetime import date, datetime, time, timedelta
from reportlab.lib.enums import TA_CENTER
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle


@login_required
def crear_reserva(request, id = None):
    """
    Vista que sirve para crear una reserva directa donde el usuario al no ingresar un recurso pero si seleccionar un tipo de recurso, automaticamente se le asigna uno de ser posible
    """
    recurso=None
    if request.method == 'POST':
        reserva_form= FormReserva(request.POST)
        if reserva_form.is_valid():
            instance = reserva_form.save(commit=False)
            instance.estado = Reserva.ACEPTADA  # add
            instance.tipo = Reserva.DIRECTA
            instance.usuario = request.user  # se agrega el usuario que realizo la reserva
            #
            #reserva_form.save()  # se guarda el form con el usuario y recurso si es que se ha seleccionado
            if instance.recurso == None:
                #print('entro aca y no deberia')
                try:
                    instance.recurso=Recurso.objects.all().filter(estado=Recurso.DISPONIBLE, tipo_recurso=instance.tipo_recurso)[:1].get()#se agrega recurso si es que no se selecciono
                except Recurso.DoesNotExist:
                    instance.recurso = None
            if instance.recurso != None:
                reserva_recurso = Recurso.objects.get(pk=instance.recurso.id)  # se obtiene el recurso asignado
                ##print(reserva_recurso)
                reservas = Reserva.objects.all().filter(estado=Reserva.ACEPTADA, recurso=reserva_recurso,fecha_reserva=datetime.now().date()).exclude(pk=instance.id)#se traen todas las reservas del dia correspondientes a ese recurso
                ##print(reservas)
                bandera=True
                for reserva in reservas:
                    if  (instance.hora_inicio >= reserva.hora_inicio and instance.hora_inicio <= reserva.hora_final) or (instance.hora_final >= reserva.hora_inicio and instance.hora_final <= reserva.hora_final):#se verifica que alguna reserva anticipada no solape la diecta
                        bandera=False #si solapa
                        ##print("ENTRO")
                        instance.estado=Reserva.RECHAZADA#todo agregar un mensaje de que no se pudo crear la reserva poruqe el recurso esta reservado para ese lapso de tiempo
                        send_mail(
                            'Reserva Rechazada',
                            'Lamentamos infomarle que su Reserva realizada ha sido Rechazada',
                            'dubium@example.com',
                            [instance.usuario.email],
                            fail_silently=False,
                        )
            else:
                bandera=False
                instance.estado=Reserva.RECHAZADA#nuevo

            if bandera:# si no solapa
                #reserva_recurso.estado = Recurso.RESERVADO
                #reserva_recurso.save()
                send_mail(
                    'Reserva Aceptada',
                    'Le informamos que la Reserva realizada ha sido Aceptada de forma exitosa. Puede retirarlo a partir de este momento. Su recurso asignado es ' + instance.recurso.nombre,
                    'dubium@example.com',
                    [instance.usuario.email],
                    fail_silently=False,
                )
                ##print(reserva_recurso.estado)
            ##print(instance.estado)
            instance.save()
            #            return redirect(reverse_lazy('modreserva:reserva_lista'))
            return render(request, 'modreserva/reserva_creado.html', {'form': reserva_form})

        # Si el form no se pudo validar por algun motivo, recuperamos el recurso
        else:
            recurso = Recurso.objects.get(id = reserva_form['recurso'].value())
    else:
        request.session['nombre_url'] = 'modreserva:crear_reserva'
        if 'recurso_id' in request.session and request.session['recurso_id']:
            recurso_id = request.session['recurso_id']
            request.session['recurso_id'] = None
            hora_inicio=request.session['hora_inicio']
            hora_final = request.session['hora_final']
            #print(hora_inicio, hora_final)
            recurso = Recurso.objects.get(id = recurso_id)
            reserva_form = FormReserva(initial={'recurso': recurso, 'tipo_recurso': recurso.tipo_recurso, 'hora_inicio': hora_inicio,'hora_final':hora_final})
        else:
            #reserva_form = FormReserva(initial={'hora_inicio': datetime.now().time().strftime('%H:%M:%S')})
            reserva_form=FormReserva()
    return render(request, 'modreserva/crear_reserva.html', {'form': reserva_form, 'recurso':recurso})

@login_required
def reserva_list(request):
    """
    Vista que sirve para listar todas las reservas en donde se muestran las distintas opciones teniendo en cuenta los permisos del usuario. Se utiliza paginator para mostrar los datos de una manera mas ordenada
    """
    #reserva_anticipada()
    valores = { # aca se van poniendo los valores para filtrar el query
         }
    arguementos = {}
    form_filtros = FormFiltros(request.GET)
    if form_filtros.is_valid():
        cd = form_filtros.cleaned_data
        valores['fecha_reserva__gte'] = cd['fecha_desde']
        valores['fecha_reserva__lte'] = cd['fecha_hasta']
        nombre_usuario = cd['usuario']
        if nombre_usuario:
            valores['usuario'] = User.objects.get(username=nombre_usuario)
        nombre_recurso = cd['recurso']
        if nombre_recurso:
            valores['recurso'] = Recurso.objects.get(nombre=nombre_recurso)
        valores['estado'] = cd['estado']
        valores['tipo'] = cd['tipo']
        valores['tipo_recurso'] = cd['tipo_recurso']
        if not request.user.has_perm('modreserva.list_all_reserva'):
            if request.user.has_perm('modreserva.list_reserva'):
                valores['tipo_recurso__profile__user'] = request.user
            else:
                valores['usuario'] = request.user

    # Codigo para filtrar el query
    for k, v in valores.items():
        if v:
            arguementos[k] = v

    pagreserva_list = Reserva.objects.filter(**arguementos).order_by('-id')


    form_reporte = FormReservaPdf(request.GET)

    page = request.GET.get('page', 1)
    paginator = Paginator(pagreserva_list, 10)
    # if 'q' in request.GET and request.GET['q']: # Che esto es necesario? pregunto porque no existe un elemento html con el id "q" ni tampoco se usa la variable "query"
    #     q = request.GET['q']
    #     reservas = Reserva.objects.filter(id=q)
    #     return render(request, 'modreserva/reserva_lista.html', {'reservas': reservas, 'query': q})
    # else:

    try:
        reservas = paginator.page(page)
    except PageNotAnInteger:
        reservas = paginator.page(1)
    except EmptyPage:
        reservas = paginator.page(paginator.num_pages)
    return render(request, 'modreserva/reserva_lista.html', {'reservas': reservas, 'form': form_filtros,'form1': form_reporte})
    #reservas = Reserva.objects.all()
    #return render(request, 'modreserva/reserva_lista.html', {'reservas': reservas})

@login_required
def reserva_list_resolver(request):
    """
    Vista que sirve para resolver las reservas anticipadas y luego mostrar una vista previa de las decisiones tomadas
    """
    reserva_anticipada()
    #vista_previa(request)
    reservas = Reserva.objects.all().filter(fecha_reserva=datetime.now().date() + timedelta(days=int(fecha_reserva)), tipo=Reserva.ANTICIPADA).exclude(estado=Reserva.EN_CURSO).exclude(estado=Reserva.FINALIZADA)
    #return render(request, 'modreserva/reserva_lista.html', {'reservas': reservas})
    return render(request, 'modreserva/vista_previa.html', {'reservas': reservas})

@login_required
def eliminar_reserva(request, parameter):
    """
    Vista que sirve para eliminar reserva, en donde el recurso pasa nuevamente a estado disponible
    """
    try:
        question = Reserva.objects.get(pk=parameter)
    except:
        question=None
        messages.error(request,"La reserva no existe")
    if question!= None and question.recurso != None:
        aux=question.recurso.id
        aux= Recurso.objects.get(pk=aux)
        aux.estado='DISPONIBLE'
        aux.save()
        question.delete()
    elif question != None:
        question.delete()
    return render(request, 'modreserva/eliminar_reserva.html')

class HoraReserva(object):
    def __init__(self, hora_inicio,hora_fin):
        self.hora_inicio=hora_inicio
        self.hora_fin=hora_fin

def reserva_anticipada():
    """
    Vista que sirve para resolver las reservas anticipadas, es la vista que decide parcialmente a quien se le otorga cada recurso y a quien no se le otroga ningun, teniendo en cuenta el cargo que uno posee y la fecha en que realizo la reserva
    """
    categorias = [Profile.TITULAR, Profile.ADJUNTO, Profile.ASISTENTE, Profile.ENCARGADO, Profile.AUXILIAR,
                  Profile.ALUMNO, Profile.FUNCIONARIO]
    tipos_recurso=TipoRecurso.objects.all()
    for tipo_recurso in tipos_recurso:
        reservas=Reserva.objects.all().filter(estado=Reserva.EN_ESPERA, tipo_recurso=tipo_recurso, fecha_reserva=datetime.now().date() + timedelta(days =int(fecha_reserva)))

        ##print(tipo_recurso)
        ##print(reservas)
        ##print(datetime.now()+timedelta(days = 2))
        recursos = Recurso.objects.all().filter(tipo_recurso=tipo_recurso)
        for recurso in recursos:
            ##print(recurso)
            horas = []
            for categoria in categorias:
                ##print(categoria)
                for reserva in reservas:
                    if reserva.estado == Reserva.EN_ESPERA:
                        #print("ACACACA")
                        #print(reserva.recurso)
                        if reserva.usuario.profile.categoria == categoria:
                            ##print("llego")
                            bandera=True
                            for hora in horas:
                                ##print("LLEGO ACA")
                                ##print(hora.hora_inicio)
                                ##print(hora.hora_fin)
                                if reserva.hora_inicio >= hora.hora_inicio and reserva.hora_inicio <= hora.hora_fin or reserva.hora_final >= hora.hora_inicio and reserva.hora_final <= hora.hora_fin:
                                    ##print("Entro al if")
                                    bandera=False
                                    break
                            if bandera and reserva.recurso == None:
                                reserva.recurso=recurso
                                reserva.estado=Reserva.ACEPTADA
                                reserva.save()
                                ##print("LLEGO AQUI")
                                horas.append(HoraReserva(reserva.hora_inicio,reserva.hora_final))
                            elif bandera and reserva.recurso == recurso:
                                #print(recurso)
                                reserva.recurso = recurso
                                reserva.estado = Reserva.ACEPTADA

                                ##print("LLEGO AQUI")
                                horas.append(HoraReserva(reserva.hora_inicio, reserva.hora_final))
                            set_recurso_as_active.apply_async(args=[reserva.recurso.id],
                                                              eta=(datetime.combine(reserva.fecha_reserva,
                                                                                    reserva.hora_inicio)) + timedelta(
                                                                  hours=4))
                            reserva.save()
    ##print("Llego a no reservas")
    no_reservas = Reserva.objects.all().filter(estado=Reserva.EN_ESPERA,fecha_reserva=datetime.now().date()+timedelta(days = int(fecha_reserva)))
    ##print(no_reservas)
    for reserva in no_reservas:
        if reserva.estado==Reserva.EN_ESPERA and reserva.tipo==Reserva.ANTICIPADA:
            reserva.estado=Reserva.RECHAZADA
            reserva.save()

@login_required
def crear_reserva_ant(request):
    """
    Vista que sirve para crear una reserva anticipada, donde la reserva creada pasa a estado EN ESPERA para luego ser aceptada o rechazada cuando se resuelven las reservas
    """
    bandera=False
    recurso = None
    if request.method == 'POST':
        reserva_form = FormReservaAnt(request.POST)
        if reserva_form.is_valid():
            instance = reserva_form.save(commit=False)
            instance.estado=Reserva.EN_ESPERA#add
            instance.tipo=Reserva.ANTICIPADA
            question =request.user  # Usuario que realizo la reserva
            instance.usuario = question
            instance.save()
            for i in range(1, instance.dias):
                instance2=Reserva.objects.get(pk=instance.id)
                instance2.fecha_reserva=instance.fecha_reserva+timedelta(days=i)
                instance2.pk=None
                instance2.save()
            return render(request, 'modreserva/reserva_creado.html', {'form': reserva_form})
    else:
        request.session['nombre_url'] = 'modreserva:crear_reserva_ant'
        if 'recurso_id' in request.session and request.session['recurso_id']:
            recurso_id = request.session['recurso_id']
            request.session['recurso_id'] = None
            hora_inicio = request.session['hora_inicio']
            hora_final = request.session['hora_final']
            fecha=request.session['fecha']
            recurso = Recurso.objects.get(id=recurso_id)
            reserva_form = FormReservaAnt(
                initial={'recurso': recurso, 'tipo_recurso': recurso.tipo_recurso, 'hora_inicio': hora_inicio,
                         'hora_final': hora_final,'fecha_reserva':fecha})
        else:
            reserva_form = FormReservaAnt(initial={'hora_inicio': datetime.now().time().strftime('%H:%M:%S')})
            # reserva_form=FormReserva()
    return render(request, 'modreserva/crear_reserva_ant.html', {'form': reserva_form,'recurso':recurso} )

@login_required
def entrega_recurso(request,parameter):
    """
    Vista que sirve para indicar que el recurso se ha entregado y devuelto, se cambia la reserva y el recurso a los estados correspondientes
    """
    try:
        reserva=Reserva.objects.get(pk=parameter)
        recurso = reserva.recurso
    except:
        reserva=None
        recurso=None
        messages.error(request,"Reserva no encontrada")

    now = datetime.now()+timedelta(minutes=int(tiempo_antes))
    now2 = datetime.now() - timedelta(minutes=int(tiempo_despues))
    now=now.strftime('%H:%M:%S')
    now2 = now2.strftime('%H:%M:%S')
    ##print (now,now2)
    #if recurso.estado==Recurso.RESERVADO:
    #elif recurso.estado==Recurso.EN_USO:
    if reserva!=None:
        if reserva.estado==Reserva.ACEPTADA and str(reserva.hora_inicio) < now and now2 < str(reserva.hora_inicio):
            reserva.estado=Reserva.EN_CURSO
            recurso.estado = Recurso.EN_USO
            reserva.fecha_hora_entrega=datetime.now()
        elif reserva.estado==Reserva.EN_CURSO:
            reserva.estado=Reserva.FINALIZADA
            recurso.estado = Recurso.DISPONIBLE
            reserva.fecha_hora_devolucion = datetime.now()
        recurso.save()
        reserva.save()
        if reserva.estado==Reserva.FINALIZADA:
            #return redirect(reverse_lazy('modreserva:crear_historial'))
            ##print(parameter, "esta es la reserva")
            return crear_historial(request,reserva)
    reservas = Reserva.objects.all()
    return render(request, 'modreserva/reserva_lista.html', {'reservas': reservas})

@login_required
def cancelar_reserva(request, parameter):
    """
Vista que sirve para cancelar una reserva, ya sea en estado en curso o Aceptada y poner dicho recurso a su estado correspondiente
    """


    reserva=Reserva.objects.get(pk=parameter)
    recurso=reserva.recurso
    if recurso.estado==Recurso.RESERVADO:
        recurso.estado=Recurso.DISPONIBLE
    if reserva.estado != Reserva.FINALIZADA:
        reserva.estado=Reserva.CANCELADA
    recurso.save()
    reserva.save()
    return reserva_list(request)
    #reservas = Reserva.objects.all()
    #return render(request, 'modreserva/reserva_lista.html', {'reservas': reservas})

@login_required
def modificar_reserva(request, parameter):
    """
    Vista utilizada para modificar una reserva en la vista previa
    """
    recurso=None
    try:
        reserva=Reserva.objects.get(pk=parameter)
    except:
        reserva=None
        messages.error(request,"La reserva no existe")
    request.session['pk'] = parameter
    if request.method == 'POST':
        form = FormModificar(instance=reserva, data=request.POST)

        if form.is_valid():

            reserva_modificada=form.save(commit=False)

            reservas = Reserva.objects.all().filter(estado=Reserva.ACEPTADA,
                                                    fecha_reserva=datetime.now().date() + timedelta(days=2)).exclude(pk=reserva_modificada.id)
            bandera=True
            for reserva in reservas:
                if (reserva.recurso == reserva_modificada.recurso) and ((reserva_modificada.hora_inicio >= reserva.hora_inicio and reserva_modificada.hora_inicio <= reserva.hora_final) or (reserva_modificada.hora_final >= reserva.hora_inicio and reserva_modificada.hora_final <= reserva.hora_final)):
                    bandera=False

            if bandera or reserva_modificada.recurso == None:
                if reserva_modificada.estado==Reserva.RECHAZADA:
                    reserva_modificada.estado=Reserva.RECHAZADA
                reserva_modificada.save()
                form.save()
            else:
                reserva_modificada.estado = Reserva.RECHAZADA
                reserva_modificada.save()
                messages.error(request, 'Error al actualizar Reserva, el recurso deseado no se encuentra disponible en ese horario')
            return redirect(reverse_lazy('modreserva:reserva_list_resolver'))

        else:
            messages.error(request, 'Error al actualizar Reserva')
    else:
        form = FormModificar()
        request.session['nombre_url'] = 'modreserva:modificar_reserva'
        request.session['reserva_id'] = parameter
        if 'recurso_id' in request.session and request.session['recurso_id']:
            recurso_id = request.session['recurso_id']
            request.session['recurso_id'] = None
            # hora_inicio = request.session['hora_inicio']
            # hora_final = request.session['hora_final']
            fecha = request.session['fecha']
            recurso = Recurso.objects.get(id=recurso_id)
            form = FormModificar(initial={'recurso': recurso, 'tipo_recurso': recurso.tipo_recurso, 'fecha_reserva': fecha, 'usuario':request.user.id,'estado':reserva.estado})
        else:
            if reserva != None:
                form = FormModificar(initial={'usuario':reserva.usuario,'recurso':reserva.recurso,'tipo_recurso':reserva.recurso.tipo_recurso,'estado':reserva.estado})
            # reserva_form=FormReserva()
    return render(request, 'modreserva/modificar_reserva.html',{'form': form, 'pk': parameter, 'recurso':recurso})

@login_required
def crear_historial(request,parameter):
    """
    Vista que sirve para que se pueda ingresa un reporte acerca de como se ha recibido el recurso luego de haber finalizado una reserva
    """
    ##print(parameter)
    ##print("crear_historial")
    if request.method == 'POST':
        ##print("entro en POST")
        form = FormHistorial(request.POST)
        if form.is_valid():
            ##print("es valido")
            reporte=form.save(commit=False)
            #question = Profile.objects.get(pk=request.user.id)  # Usuario que realizo la reserva
            reporte.usuario = request.user
            ##print(reporte.reserva,parameter)
            reporte.reserva=parameter
            reporte.recurso=parameter.recurso
            parameter.recurso.estado=reporte.estado#se copia el estado del recurso al ingresado en el reporte
            #print(parameter.recurso.estado)
            #reporte.recurso.estado=parameter.recurso.estado
            #reporte.reserva=reserva
            reporte.save()
            parameter.recurso.save()#se actualiza el estado del recurso
            ##print(reporte.usuario,reporte.reserva)
            return render(request, 'modreserva/historial_agregado.html', {'form': form})
    else:
        ##print("entro en el else")
        form = FormHistorial(initial={'usuario_dev':request.user})
    return render(request, 'modreserva/crear_historial.html', {'form': form})

@login_required
def buscar_recurso_lista(request):
    """
    Vista utilizada para listar todos recursos (model: `modrecurso.Recurso`) que pertenecen a la misma categoria (model: `modrecurso:Categoria`) que el usuario (model: `auth:User`)
    """
    bandera=None
    bandera_fecha=None
    fecha=None
    hora_inicio=datetime.now().strftime('%H:%M:%S')
    ##print(hora_inicio)
    hora_final = None
    recurso_list = None
    pagrecurso_list = None
    if request.method == 'GET':
        form = BuscarRecursoForm(request.GET)
        if form.is_valid():
            cd = form.cleaned_data
            # ahora en cd, se tiene el form como diccionario.
            # Datos necesarios para pasar las horas del form busqueda al form llamador
            hora_inicio = cd.get('hora_inicio')
            hora_final = cd.get('hora_final')
            b = cd.get('nombre')
            #print(b,"nombre")
            c = cd.get('area')
            #print(c,"area")
            d = cd.get('tipo_recurso')
            #print(d,"tipo de recurso")
            e = cd.get('hora_inicio')
            #print(e)
            f = cd.get('hora_final')
            #print(f)
            g = cd.get('fecha')
            #print(g)
            if b and c and d:
                pagrecurso_list = Recurso.objects.filter(nombre__icontains=b, area=c, tipo_recurso=d).exclude(estado=Recurso.DESCOMPUESTO).exclude(estado=Recurso.DESCONOCIDO).exclude(estado=Recurso.MANTENIMIENTO).exclude(estado=Recurso.INUTILIZABLE).order_by('tipo_recurso__nombre', 'nombre')
            elif b and c:
                pagrecurso_list = Recurso.objects.filter(nombre__icontains=b, area=c).exclude(estado=Recurso.DESCOMPUESTO).exclude(estado=Recurso.DESCONOCIDO).exclude(estado=Recurso.MANTENIMIENTO).exclude(estado=Recurso.INUTILIZABLE).order_by('tipo_recurso__nombre', 'nombre')
            elif b and d:
                pagrecurso_list = Recurso.objects.filter(nombre__icontains=b, tipo_recurso=d).exclude(estado=Recurso.DESCOMPUESTO).exclude(estado=Recurso.DESCONOCIDO).exclude(estado=Recurso.MANTENIMIENTO).exclude(estado=Recurso.INUTILIZABLE).order_by('tipo_recurso__nombre', 'nombre')
            elif c and d:
                pagrecurso_list = Recurso.objects.filter(area=c, tipo_recurso=d).exclude(estado=Recurso.DESCOMPUESTO).exclude(estado=Recurso.DESCONOCIDO).exclude(estado=Recurso.MANTENIMIENTO).exclude(estado=Recurso.INUTILIZABLE).order_by('tipo_recurso__nombre', 'nombre')
            elif b:
                pagrecurso_list = Recurso.objects.filter(nombre__icontains=b).exclude(estado=Recurso.DESCOMPUESTO).exclude(estado=Recurso.DESCONOCIDO).exclude(estado=Recurso.MANTENIMIENTO).exclude(estado=Recurso.INUTILIZABLE).order_by('tipo_recurso__nombre', 'nombre')
            elif c:
                pagrecurso_list = Recurso.objects.filter(area=c).exclude(estado=Recurso.DESCOMPUESTO).exclude(estado=Recurso.DESCONOCIDO).exclude(estado=Recurso.MANTENIMIENTO).exclude(estado=Recurso.INUTILIZABLE).order_by('tipo_recurso__nombre', 'nombre')
            elif d:
                ##print(d)
                pagrecurso_list = Recurso.objects.filter(tipo_recurso=d).exclude(estado=Recurso.DESCOMPUESTO).exclude(estado=Recurso.DESCONOCIDO).exclude(estado=Recurso.MANTENIMIENTO).exclude(estado=Recurso.INUTILIZABLE).order_by('tipo_recurso__nombre', 'nombre')
                ##print(pagrecurso_list,"entro d")
            else:
                pagrecurso_list = Recurso.objects.filter().exclude(estado=Recurso.DESCOMPUESTO).exclude(estado=Recurso.DESCONOCIDO).exclude(estado=Recurso.MANTENIMIENTO).exclude(estado=Recurso.INUTILIZABLE).order_by('tipo_recurso__nombre', 'nombre')
            ##print(pagrecurso_list)
            fecha=g
            nombre_url = request.session['nombre_url']
            if g == datetime.now().date() and nombre_url !='modreserva:modificar_reserva':
                #print("entro en el if de buscar")
                try:
                    ####
                    to_be_deleted = []
                    for recurso in pagrecurso_list:
                        reservas=Reserva.objects.all().filter(recurso=recurso,fecha_reserva=datetime.now().date()).exclude(estado=Reserva.FINALIZADA)
                        ##print(reservas)
                        for reserva in reservas:
                            if (e >= reserva.hora_inicio and e <= reserva.hora_final) or (
                                    f >= reserva.hora_inicio and f <= reserva.hora_final):  # se verifica que alguna reserva anticipada no solape la diecta
                                to_be_deleted.append(recurso.id)
                                ##print(recurso.nombre)
                                pagrecurso_list=pagrecurso_list.exclude(id__in=to_be_deleted)
                                ##print(pagrecurso_list)
                    ####
                except:
                    print("No se realizo")
                    pass

        page = request.GET.get('page', 1)
        paginator = Paginator(pagrecurso_list, 15)

        try:
            recurso_list = paginator.page(page)
        except PageNotAnInteger:
            recurso_list = paginator.page(1)
        except EmptyPage:
            recurso_list = paginator.page(paginator.num_pages)


    elif request.method == 'POST':
        ##print(request.POST)
        recurso_id = request.POST.get('recurso_id')
        request.session['recurso_id'] = recurso_id
        hora_inicio=request.POST.get('hora_inicio')
        request.session['hora_inicio'] = hora_inicio
        hora_final = request.POST.get('hora_final')
        request.session['hora_final'] = hora_final
        fecha = request.POST.get('fecha')
        request.session['fecha'] = fecha
        #args = request.POST.get('pk')
        #args = request.session['pk'] revisar esto, lo movi dentro del if nombre_url =='modreserva...'
        ##print(args)
        # Returns None if user came from another website
        nombre_url = request.session['nombre_url']
        #print(nombre_url)
        ##print(recurso_id)
        ##print("Horas")
        ##print(hora_inicio)
        ##print(hora_final)
        if nombre_url =='modreserva:modificar_reserva':
            args = request.session['pk']
            return redirect(reverse_lazy('modreserva:modificar_reserva',kwargs={'parameter': args}))
        else:
            return redirect(reverse_lazy(nombre_url))

    nombre_url = request.session['nombre_url']

    #para mostrar fecha,hora solo si es necesario
    if nombre_url =='modreserva:modificar_reserva':
            reserva_id = request.session['reserva_id']
            #print("entro entro")
            bandera=False
    else:
        ##print(nombre_url)
        reserva_id = None

        if nombre_url == 'modreserva:crear_reserva':
            bandera=True
            bandera_fecha=False
        else:
            bandera=True
            bandera_fecha=True
    #print(bandera)

    return render(request, 'modreserva/buscar_recurso_list.html', {'form': form, 'recurso_list': recurso_list, 'nombre_url': nombre_url,'hora_inicio':hora_inicio,'hora_final':hora_final,'fecha':fecha,'bandera':bandera,'bandera_fecha':bandera_fecha, 'reserva_id': reserva_id})

@login_required
def recuperar(request,parameter):
    """
    Vista que sirve para cargar datos de retiro-devolucion en el caso de que el sistema no este disponible
    """
    ##print(parameter)
    ##print("crear_historial")
    parameter = Reserva.objects.get(pk=parameter)
    if request.method == 'POST':
        ##print("entro en POST")
        form1 = FormRecuperar(request.POST,instance=parameter)
        form2 = FormHistorial(request.POST)
        if form1.is_valid() and form2.is_valid():
            ##print("es valido")
            reporte=form2.save(commit=False)
            #question = Profile.objects.get(pk=request.user.id)  # Usuario que realizo la reserva
            reporte.usuario = request.user
            ##print(reporte.reserva,parameter)

            reporte.reserva=parameter
            reporte.recurso=parameter.recurso
            parameter.recurso.estado=reporte.estado#se copia el estado del recurso al ingresado en el reporte
            #print(parameter.recurso.estado)
            #reporte.recurso.estado=parameter.recurso.estado
            #reporte.reserva=reserva
            reporte.save()
            form1.save()
            parameter.recurso.save()#se actualiza el estado del recurso
            ##print(reporte.usuario,reporte.reserva)
            return render(request, 'modreserva/historial_agregado.html', {'form': form2})
    else:
        ##print("entro en el else")
        form1 = FormRecuperar()
        form2 = FormHistorial(initial={'usuario_dev':request.user})
    return render(request, 'modreserva/crear_historial.html', {'form': form2,'form1':form1})


class ReporteReservasPDF(View):
    def cabecera(self, pdf, reserva_tabla):
        styles = getSampleStyleSheet()
        style1 = ParagraphStyle(
            name='Normal',
            fontName='Helvetica-Bold',
            fontSize=16,
            alignment=TA_CENTER,
        )
        style2 = ParagraphStyle(
            name='Normal',
            fontName='Helvetica',
            fontSize=14,
            alignment=TA_CENTER,
        )
        style3 = ParagraphStyle(
            name='Normal',
            fontName='Helvetica',
            fontSize=7,
            alignment=TA_CENTER,
        )

        logo = "static/img/dubium.png"
        im = Image(logo, 1.5 * inch, 0.4 * inch)
        im.hAlign = 'LEFT'
        reserva_tabla.append(im)
        reserva_tabla.append(Spacer(1, 0.2 * cm))
        reserva_tabla.append(Paragraph("SISTEMA DE GESTION DE RECURSOS", style1))
        reserva_tabla.append(Spacer(1, 0.2 * cm))
        reserva_tabla.append(Paragraph("REPORTE DE RESERVAS", style2))
        reserva_tabla.append(Spacer(1, 0.2 * cm))
        reserva_tabla.append(Paragraph("Generado el " + datetime.now().strftime('%d de %b del %Y a las %H:%M:%S'), style3))
        reserva_tabla.append(Spacer(1, 0.2 * cm))



    def get(self, request, *args, **kwargs):
        valores1 = {  # aca se van poniendo los valores para filtrar el query
        }
        arguementos1 = {}
        form_reporte = FormReservaPdf(request.GET)
        if form_reporte.is_valid():
            cd = form_reporte.cleaned_data
            valores1['fecha_reserva__gte'] = cd['fecha_desde']
            valores1['fecha_reserva__lte'] = cd['fecha_hasta']
            valores1['estado'] = cd['estado']
            valores1['tipo'] = cd['tipo']
            valores1['tipo_recurso'] = cd['tipo_recurso']
            if not request.user.has_perm('modreserva.list_all_reserva'):
                if request.user.has_perm('modreserva.list_reserva'):
                    valores1['tipo_recurso__profile__user'] = request.user
                else:
                    valores1['usuario'] = request.user

        # Codigo para filtrar el query
        for k, v in valores1.items():
            if v:
                arguementos1[k] = v

        reserva_list = Reserva.objects.filter(**arguementos1).order_by('-id')
        # Indicamos el tipo de contenido a devolver, en este caso un pdf
        response = HttpResponse(content_type='application/pdf')
        # La clase io.BytesIO permite tratar un array de bytes como un fichero binario, se utiliza como almacenamiento temporal
        buffer = BytesIO()
        doc = SimpleDocTemplate(buffer,
                                pagesize=letter,
                                rightMargin=30,
                                leftMargin=30,
                                topMargin=20,
                                bottomMargin=18,
                                )
        # Creamos una lista auxiliar
        reserva_tabla = []
        pdf = canvas.Canvas("reserva.pdf", pagesize=letter)
        # Llamo al método cabecera donde están definidos los datos que aparecen en la cabecera del reporte.
        self.cabecera(pdf, reserva_tabla)
        y = 500
        # Llamo al método tabla donde están las tablas  que aparecen en el cuerpo del reporte.
        t = self.tabla(pdf, y, reserva_list)
        reserva_tabla.append(t)
        doc.build(reserva_tabla)
        # Con show page hacemos un corte de página para pasar a la siguiente
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response

        print(reserva_list)

        # Indicamos el tipo de contenido a devolver, en este caso un pdf
        response = HttpResponse(content_type='application/pdf')
        # La clase io.BytesIO permite tratar un array de bytes como un fichero binario, se utiliza como almacenamiento temporal
        buffer = BytesIO()
        # Canvas nos permite hacer el reporte con coordenadas X y Y
        pdf = canvas.Canvas(buffer)
        # Llamo al método cabecera donde están definidos los datos que aparecen en la cabecera del reporte.
        self.cabecera(pdf)
        y = 600
        self.tabla(pdf, y,reserva_list)
        # Con show page hacemos un corte de página para pasar a la siguiente
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response


    def tabla(self, pdf, y,reserva_list):
        # Creamos una tupla de encabezados para nuestra tabla
        encabezados = ('ID','Recurso', 'Tipo de Recurso','Estado del Recurso','Area','Fecha de Reserva', 'Responsable','Estado de Reserva')
        # Creamos una lista de tuplas que van a contener a las personas

        detalles = [(Reserva.id, Reserva.recurso,Reserva.tipo_recurso,Reserva.recurso.estado,Reserva.recurso.area,Reserva.fecha_reserva,Reserva.usuario,Reserva.estado) for Reserva in
                    reserva_list]

        # Establecemos el tamaño de cada una de las columnas de la tabla
        detalle_orden = Table([encabezados] + detalles, colWidths=[0.9*cm,2.5*cm,4*cm,2*cm,4*cm,2*cm,2*cm,2*cm])
        # Aplicamos estilos a las celdas de la tabla
        detalle_orden.setStyle(TableStyle(
            [
                # La primera fila(encabezados) va a estar centrada
                ('ALIGN', (0, 0), (7, 0), 'CENTER'),
                ('BACKGROUND', (0, 0), (7, 0), 'GRAY'),
                ('TEXTCOLOR', (0, 0), (7, 0), 'WHITE'),
                ('FONTNAME', (0, 0), (7, 0), 'Helvetica-Bold'),
                ('TOPPADDING', (0, 0), (7, 0), 7),
                # Los bordes de todas las celdas serán de color negro y con un grosor de 1
                ('GRID', (0, 0), (-1, -1), 1, colors.black),
                # El tamaño de las letras de cada una de las celdas será de 5
                ('FONTSIZE', (0, 0), (-1, -1), 5),
            ]
        ))
        # Establecemos el tamaño de la hoja que ocupará la tabla
        detalle_orden.wrapOn(pdf, 800, 600)
        # Definimos la coordenada donde se dibujará la tabla
        detalle_orden.drawOn(pdf, 24, y)
        return detalle_orden

