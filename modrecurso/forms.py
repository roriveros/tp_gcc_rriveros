from django import forms
from .models import Recurso, Caracteristica, TipoRecurso
from modusuario.models import Area, Profile
from django.contrib.auth.models import User


class FormRecurso(forms.ModelForm):
    """
    Formulario utilizado para la creacion de un recurso (model: `modrecurso.Recurso`)
    """
    class Meta:
        model = Recurso
        fields = ('nombre', 'estado', 'area', 'tipo_recurso')

class EditarForm(forms.ModelForm):
    """
        Formulario utilizado para la edicion de un recurso (model: `modrecurso.Recurso`)
    """
    class Meta:
        model = Recurso
        fields = ('nombre','estado', 'area', 'tipo_recurso')

class CaracteristicaCreateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        recurso_id = kwargs.pop('recurso_id')
        super(CaracteristicaCreateForm, self).__init__(*args, **kwargs)
        try:
            self.fields['nombre'] = forms.ModelChoiceField(required=True, queryset=Recurso.objects.get(id=recurso_id).tipo_recurso.caracteristicatiporecurso_set.all())
        except:
            self.fields['nombre'] = forms.ModelChoiceField(required=True, queryset=Recurso.objects.all()[:1].get().tipo_recurso.caracteristicatiporecurso_set.all())
    class Meta:
        model = Caracteristica
        fields = ('nombre', 'descripcion')

class FormFiltrosRecurso(forms.Form):

    nombre = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        super(FormFiltrosRecurso, self).__init__(*args, **kwargs)
        ESTADO_CHOICES = (('', 'Todos'),) + Recurso.ESTADO_CHOICES

        self.fields['tipo_recurso'] = forms.ModelChoiceField(required=False, queryset=TipoRecurso.objects.all(),
                                                             empty_label='Todos')
        self.fields['estado'] = forms.ChoiceField(required=False, choices=ESTADO_CHOICES)
        self.fields['area'] = forms.ModelChoiceField(required=False, queryset=Area.objects.all(), empty_label='Todas')

class FormReportePdf(forms.Form):

    def __init__(self, *args, **kwargs):
        super(FormReportePdf, self).__init__(*args, **kwargs)
        ESTADO_CHOICES = (('', 'Todos'),) + Recurso.ESTADO_CHOICES


        self.fields['tipo_recurso'] = forms.ModelChoiceField(required=False, queryset=TipoRecurso.objects.all(),empty_label='Todos')
        self.fields['estado'] = forms.ChoiceField(required=False, choices=ESTADO_CHOICES)
        self.fields['area'] = forms.ModelChoiceField(required=False, queryset=Area.objects.all(), empty_label='Todas')