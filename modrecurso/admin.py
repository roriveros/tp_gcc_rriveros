from django.contrib import admin

from .models import Recurso,TipoRecurso,Caracteristica

admin.site.register(Recurso)
admin.site.register(TipoRecurso)
#admin.site.register(Departamento)
admin.site.register(Caracteristica)
#admin.site.register(Mantenimiento)
