from django.shortcuts import render_to_response
from .forms import FormRecurso,EditarForm, CaracteristicaCreateForm, FormFiltrosRecurso, FormReportePdf
from .models import Recurso, Caracteristica, TipoRecurso, CaracteristicaTipoRecurso
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy
from modreserva.models import Reserva
from datetime import datetime
from django.core.mail import send_mail
from django.contrib.auth.decorators import login_required


from django.conf import settings
from io import BytesIO
from reportlab.pdfgen import canvas
from django.views.generic import View
from django.http import HttpResponse
from django.views.generic import ListView
from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib import colors
from reportlab.platypus import Table
from reportlab.lib.units import cm, inch
from datetime import date, datetime, time, timedelta
from reportlab.lib.enums import TA_CENTER
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle


@login_required
def crear_recurso(request):
    """
    Vista que se utiliza para crear un recurso con modelo `modrecurso.Recurso`
    """
    if request.method == 'POST':
        recurso_form= FormRecurso(request.POST)
        if recurso_form.is_valid():
            recurso_form.save()

            return render(request, 'modrecurso/recurso_creado.html', {'form': recurso_form})

           # return render(request, 'modrecurso/recurso_creado.html', {'new_recurso': recurso_form})
    else:
        recurso_form=FormRecurso()
    return render(request, 'modrecurso/crear_recurso.html', {'form':recurso_form})

# def recurso_list(request):
#     """
#     Vista que se utiliza para listar todos los recursos del modelo `moderecurso.Recurso`
#     """
#     recursos=Recurso.objects.order_by('id')
#     return render_to_response('modrecurso/recurso_list.html', {'recursos': recursos})

@login_required
def eliminar_recurso(request, parameter):
    """
    Vista utilizada para eliminar un recurso con el modelo `modrecurso.Recurso`
    """
    question = Recurso.objects.get(pk=parameter)
    question.delete()
    return render(request, 'modrecurso/eliminar_recurso.html')

class RecursoDelete(DeleteView):
    """
    Vista que sirve para eliminar un `auth.User`
    """
    model = Recurso
    template_name = 'modrecurso/recurso_confirm_delete.html'

    def get_success_url(self):
        return reverse('modrecurso:buscar')

@login_required
def modificar_recurso(request, parameter):
    """
    Vista utilizada para modificar un recurso con el modelo `modrecurso.Recurso` 
    """
    try:
        recurso = Recurso.objects.get(pk=parameter)
        caracteristicas = recurso.caracteristica_set.all()
    except:
        caracteristicas=None
        recurso=None
        messages.error(request,"El recurso no existe")
    if request.method == 'POST':
        form = EditarForm(instance=recurso, data=request.POST)
        if form.is_valid():
            reserva_form=form.save(commit=False)
            if reserva_form.estado == Recurso.DESCOMPUESTO:
                reservas = Reserva.objects.all().filter(recurso=recurso, fecha_reserva=datetime.now().date()).exclude(
                    estado=Reserva.FINALIZADA)
                for reserva in reservas:
                    reserva.estado = Reserva.CANCELADA
                    reserva.save()
                    send_mail(
                        'Reserva Rechazada',
                        'Lamentamos infomarle que su Reserva realizada ha sido Rechazada porque el recurso se ha descompuesto',
                        'dubium@example.com',
                        [reserva.usuario.user.email],
                        fail_silently=False,
                    )
            reserva_form.save()
            return redirect(reverse_lazy('modrecurso:buscar'))

        else:
            messages.error(request, 'Error al actualizar Recurso')
    else:
        form = EditarForm(instance=recurso)
    return render(request, 'modrecurso/modificar_recurso.html', {'form': form, 'caracteristicas': caracteristicas, 'pk': parameter})

# def search(request):
#     if 'q' in request.GET and request.GET['q']:
#         q = request.GET['q']
#         recursos = Recurso.objects.filter(id=q)
#         return render(request, 'modrecurso/recurso_list.html', {'recursos': recursos, 'query': q})
#     else:
#         recursos = Recurso.objects.order_by('id')
#         return render(request, 'modrecurso/recurso_list.html',{'recursos': recursos})

@login_required
def recurso_search(request):
    """
    Vista utilizada para listar todos recursos (model: `modrecurso.Recurso`) que pertenecen a la misma categoria (model: `modrecurso:Categoria`) que el usuario (model: `auth:User`)
    """

    valores = { # aca se van poniendo los valores para filtrar el query
         }
    arguementos = {}
    form_filtros = FormFiltrosRecurso(request.GET)
    if form_filtros.is_valid():
        cd = form_filtros.cleaned_data
        valores['nombre__icontains'] = cd['nombre']
        valores['tipo_recurso'] = cd['tipo_recurso']
        valores['estado'] = cd['estado']
        valores['area'] = cd['area']
        if not request.user.has_perm('modrecurso.list_all_recurso'):
            valores['area__profile__user'] = request.user
            valores['tipo_recurso__profile__user'] = request.user

    # Codigo para filtrar el query
    for k, v in valores.items():
        if v:
            arguementos[k] = v

    pagrecurso_list = Recurso.objects.filter(**arguementos).order_by('tipo_recurso__nombre', 'nombre')

    page = request.GET.get('page', 1)
    paginator = Paginator(pagrecurso_list, 15)

    form_reporte = FormReportePdf(request.GET)

    try:
        recursos = paginator.page(page)
    except PageNotAnInteger:
        recursos = paginator.page(1)
    except EmptyPage:
        recursos = paginator.page(paginator.num_pages)
    return render(request, 'modrecurso/recurso_lista.html', {'recursos': recursos, 'form': form_filtros,'form1':form_reporte})

# def caracteristicas_list(request, parameter):
#     """
#     Vista utilizada para listar las caracteristicas del modelo `modrecurso:Caracteristica`
#     """
#     recurso = Recurso.objects.get(id=parameter)
#     caracteristicas=recurso.caracteristica_set.all()
#     return render_to_response('modrecurso/listar_caracteristicas.html', {'caracteristicas': caracteristicas})

@login_required
def caracteristicas_tiporecurso_list(request, parameter):
    """
    Vista utilizada para listar las caracteristicas del modelo `modrecurso:Caracteristica_TipoRecurso` 
    """
    tipo_recurso = TipoRecurso.objects.get(id=parameter)
    caracteristicas=tipo_recurso.caracteristicatiporecurso_setall()
    return render_to_response('modrecurso/caracteristica_tiporecurso_list.html', {'caracteristicas': caracteristicas})


class TipoRecursoList(ListView):
    """
    Vista utilizada para listar todos los tipos de recurso del modelo `modrecurso.TipoRecurso`
    """
    model = TipoRecurso
    paginate_by = 15
    context_object_name = 'tiporecurso_list'
    template_name = 'modrecurso/tiporecurso_list.html'
    ordering = ['id']

class TipoRecursoCreate(CreateView):
    """
    Vista utilizada para crear un  tipo de recurso con el modelo `modrecurso.TipoRecurso`
    """
    model = TipoRecurso
    fields = ['nombre', 'descripcion']
    template_name = 'modrecurso/tiporecurso_create.html'

    def get_success_url(self):
        return reverse('modrecurso:tiporecurso_list')

class TipoRecursoUpdate(UpdateView):
    """
    Vista utilizada para actualizar un tipo de recurso con el modelo `modrecurso.TipoRecurso`
    """
    model = TipoRecurso
    fields = ['nombre', 'descripcion']
    template_name = 'modrecurso/tiporecurso_update.html'

    def get_success_url(self):
        return reverse('modrecurso:tiporecurso_list')

    def get_context_data(self, **kwargs):
        context = super(TipoRecursoUpdate, self).get_context_data(**kwargs)
        context['caracteristicas'] = self.object.caracteristicatiporecurso_set.all()
        context['pk'] = self.object.id
        return context

class TipoRecursoDelete(DeleteView):
    """
    Vista utilizada para eliminar un tipo de recurso con el modelo `modrecurso.TipoRecurso`
    """
    model = TipoRecurso
    def get_success_url(self):
        return reverse('modrecurso:tiporecurso_list')

class CaracteristicaCreate(CreateView):
    """
    Vista utilizada para crear una caracteristica con el modelo `modrecurso.Caracteristica`
    """
    model = Caracteristica
    form_class = CaracteristicaCreateForm
    template_name = 'modrecurso/caracteristica_create.html'

    def form_valid(self, form):
        form.instance.recurso = Recurso.objects.get(id=self.kwargs['pk'])
        return super(CaracteristicaCreate, self).form_valid(form)

    def get_success_url(self):
        return reverse('modrecurso:modificar_recurso', kwargs={'parameter' : self.kwargs['pk']})

    def get_form_kwargs(self):
        """"Se le pasa al form de caracteristica el id del recurso"""
        kwargs = super(CaracteristicaCreate, self).get_form_kwargs()
        kwargs['recurso_id'] = self.kwargs['pk']
        return kwargs

class CaracteristicaTipoRecursoCreate(CreateView):
    """
    Vista utilizada para crear una caracteristica con el modelo `modrecurso.Caracteristica`
    """
    model = CaracteristicaTipoRecurso
    fields = ['nombre']
    template_name = 'modrecurso/caracteristica_tiporecurso_create.html'

    def form_valid(self, form):
        form.instance.tipo_recurso = TipoRecurso.objects.get(id=self.kwargs['pk'])
        return super(CaracteristicaTipoRecursoCreate, self).form_valid(form)

    def get_success_url(self):
        return reverse('modrecurso:tiporecurso_update', kwargs={'pk' : self.kwargs['pk']})


class ReporteRecursosPDF(View):
    def cabecera(self,pdf,recurso_tabla):
        styles = getSampleStyleSheet()
        style1 = ParagraphStyle(
            name='Normal',
            fontName='Helvetica-Bold',
            fontSize=16,
            alignment=TA_CENTER,
        )
        style2 = ParagraphStyle(
            name='Normal',
            fontName='Helvetica',
            fontSize=14,
            alignment=TA_CENTER,
        )
        style3 = ParagraphStyle(
            name='Normal',
            fontName='Helvetica',
            fontSize=7,
            alignment=TA_CENTER,
        )

        logo = "static/img/dubium.png"
        im = Image(logo, 1.5 * inch, 0.4 * inch)
        im.hAlign = 'LEFT'
        recurso_tabla.append(im)
        recurso_tabla.append(Spacer(1, 0.2 * cm))
        recurso_tabla.append(Paragraph("SISTEMA DE GESTION DE RECURSOS", style1))
        recurso_tabla.append(Spacer(1, 0.2 * cm))
        recurso_tabla.append(Paragraph("REPORTE DE RECURSOS", style2))
        recurso_tabla.append(Spacer(1, 0.2 * cm))
        recurso_tabla.append(
            Paragraph("Generado el " + datetime.now().strftime('%d de %b del %Y a las %H:%M:%S'), style3))
        recurso_tabla.append(Spacer(1, 0.2 * cm))

    def get(self, request, *args, **kwargs):
        valores1 = {  # aca se van poniendo los valores para filtrar el query
        }
        arguementos1 = {}
        form_reporte = FormReportePdf(request.GET)

        if form_reporte.is_valid():
            cd = form_reporte.cleaned_data
            valores1['tipo_recurso'] = cd['tipo_recurso']
            valores1['estado'] = cd['estado']
            valores1['area'] = cd['area']
            if not request.user.has_perm('modrecurso.list_all_recurso'):
                valores1['area__profile__user'] = request.user
                valores1['tipo_recurso__profile__user'] = request.user

        # Codigo para filtrar el query
        for k, v in valores1.items():
            if v:
                arguementos1[k] = v

        recurso_list = Recurso.objects.filter(**arguementos1).order_by('tipo_recurso__nombre', 'nombre')
        print(recurso_list)

        # Indicamos el tipo de contenido a devolver, en este caso un pdf
        response = HttpResponse(content_type='application/pdf')
        # La clase io.BytesIO permite tratar un array de bytes como un fichero binario, se utiliza como almacenamiento temporal
        buffer = BytesIO()
        doc = SimpleDocTemplate(buffer,
                                pagesize=letter,
                                rightMargin=30,
                                leftMargin=30,
                                topMargin=20,
                                bottomMargin=18,
                                )
        # Creamos una lista auxiliar
        recurso_tabla = []
        # Con canvas hacemos el reporte con coordenadas X y Y
        pdf = canvas.Canvas("recurso.pdf",pagesize=letter)
        self.cabecera(pdf,recurso_tabla)
        y = 500
        t= self.tabla(pdf, y,recurso_list)

        recurso_tabla.append(t)
        doc.build(recurso_tabla)
        # Con show page hacemos un corte de página para pasar a la siguiente
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response


    def tabla(self, pdf, y,recurso_list):
        # Creamos una tupla de encabezados para nuestra tabla
        encabezados = ('ID', 'Nombre', 'Tipo de Recurso', 'Estado', 'Area')
        # Creamos una lista de tuplas que van a contener a las personas
        detalles = [(Recurso.id, Recurso.nombre,Recurso.tipo_recurso, Recurso.estado, Recurso.area)  for Recurso in recurso_list]
        # Establecemos el tamaño de cada una de las columnas de la tabla
        detalle_orden = Table([encabezados] + detalles, colWidths=[1*cm, 3*cm, 4*cm,3*cm, 5*cm])
        # Aplicamos estilos a las celdas de la tabla
        detalle_orden.setStyle(TableStyle(
            [
                # La primera fila(encabezados) va a estar centrada
                ('ALIGN', (0, 0), (4, 0), 'CENTER'),
                ('BACKGROUND', (0, 0), (4, 0), 'GRAY'),
                ('TEXTCOLOR', (0, 0), (4, 0), 'WHITE'),
                ('FONTNAME', (0, 0), (4, 0), 'Helvetica-Bold'),
                ('TOPPADDING', (0, 0), (4, 0), 7),
                # Los bordes de todas las celdas serán de color negro y con un grosor de 1
                ('GRID', (0, 0), (-1, -1), 1, colors.black),
                # El tamaño de las letras de cada una de las celdas será de 5
                ('FONTSIZE', (0, 0), (-1, -1), 5),
            ]
        ))
        # Establecemos el tamaño de la hoja que ocupará la tabla
        detalle_orden.wrapOn(pdf, 800, 600)
        # Definimos la coordenada donde se dibujará la tabla
        detalle_orden.drawOn(pdf, 24, 500)
        return detalle_orden

