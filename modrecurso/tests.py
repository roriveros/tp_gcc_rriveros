from django.test import TestCase
from .models import TipoRecurso, Recurso
from django.contrib.auth.models import User
from modusuario.models import Area, Profile
from modmantenimiento.models import Mantenimiento
import datetime
from django.core.urlresolvers import reverse_lazy


# Create your tests here.

class TipoRecursoCase(TestCase):
    """ Prueba unitaria que sirve para probar el almaceniamiento y consulta de un recurso y sus atributos respectivos"""

    @classmethod
    def setUpTestData(cls):
        """Inicializa los datos y guarda en la base de datos"""
        user = User.objects.create_user('username', 'username@gmail.com', 'password')
        profile = Profile.objects.create(user=user, direccion='La esquina', ci='1.234.567')
        a1 = Area.objects.create(nombre='LabR01', descripcion='Laboratorio de Redes Nro 1')
        a2 = Area.objects.create(nombre='LabR02', descripcion='Laboratorio de Redes Nro 2')
        t1 = TipoRecurso.objects.create(nombre='Aula', descripcion='Descripcion Proyector2')
        t2 = TipoRecurso.objects.create(nombre='Mesa', descripcion='Descripcion Monitor1')
        Recurso.objects.create(nombre='aula01', estado=Recurso.DISPONIBLE, area=a1, tipo_recurso=t1)
        Recurso.objects.create(nombre='mesa02', estado=Recurso.DESCOMPUESTO, area=a2, tipo_recurso=t2)


    def test_recurso_tiporecurso(self):
        """Prueba si la relacion es correcta entre el recurso y tipo de recurso"""
        recurso1 = Recurso.objects.get(nombre='aula01')
        self.assertEqual(recurso1.tipo_recurso.nombre,'Aula', 'Fallo la consulta a la base de datos al obtener el recurso o el tipo de recurso o la relacion no es correcta')

    def test_recurso2_tiporecurso(self):
        """Prueba si la relacion es correcta entre el recurso y tipo de recurso"""
        recurso2 = Recurso.objects.get(nombre='mesa02')
        self.assertEqual(recurso2.tipo_recurso.nombre,'Mesa', 'Fallo la consulta a la base de datos al obtener el recurso o el tipo de recurso o la relacion no es correcta')

    def test_recurso_area_nombre_max_length(self):
        """Prueba si la maxima longitud de el campo es valida"""
        recurso1 = Recurso.objects.get(nombre='aula01')
        max_length = recurso1.area._meta.get_field('nombre').max_length
        self.assertEquals(max_length, 100,
                          'Fallo la consula a la base de datos al obtener la longitud maxima o esta no es valida')

    def test_recurso_area_descripcion_max_length(self):
        """Prueba si la maxima longitud de el campo es valida"""
        recurso1 = Recurso.objects.get(nombre='aula01')
        max_length = recurso1.area._meta.get_field('descripcion').max_length
        self.assertEquals(max_length, 2000,
                          'Fallo la consula a la base de datos al obtener la longitud maxima o esta no es valida')

    def test_recurso_area1(self):
        """Prueba si la relacion es correcta entre el recurso y el area"""
        recurso1 = Recurso.objects.get(nombre='aula01')
        area = Area.objects.get(id=recurso1.area.id)
        self.assertEqual(area.nombre, 'LabR01', 'Fallo la consulta a la base de datos al obtener el recurso o el mantenimiento o la relacion no es correcta')

    def test_recurso_area(self):
        """Prueba si la relacion es correcta entre el recurso y el area"""
        recurso2 = Recurso.objects.get(nombre='mesa02')
        area = Area.objects.get(id=recurso2.area.id)
        self.assertEqual(area.nombre, 'LabR02', 'Fallo la consulta a la base de datos al obtener el recurso o el mantenimiento o la relacion no es correcta')

    def test_recurso_create(self):
        """"Debe mostrar la pagina para crear recurso"""
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modrecurso:crear_recurso'))
        self.assertEqual(response.status_code, 200, 'error no se pudo crear el recurso')


    def test_recurso_modificar(self):
        """"Debe mostrar la pagina para modificar recurso"""
        user = User.objects.get(username='username')
        self.client.login(username='username', password='password')
        tipo = TipoRecurso.objects.all()[:1].get()
        response = self.client.get(reverse_lazy('modrecurso:modificar_recurso', args={tipo.id}))
        self.assertEqual(response.status_code, 200, 'error no se pudo modificar recurso')

    def test_recurso_eliminar(self):
        """"Debe mostrar la pagina para eliminar recurso"""
        user = User.objects.get(username='username')
        self.client.login(username='username', password='password')
        tipo = TipoRecurso.objects.all()[:1].get()
        response = self.client.get(reverse_lazy('modrecurso:tiporecurso_delete', kwargs={'pk': tipo.id}))
        self.assertEqual(response.status_code, 200, 'error no se pudo eliminar recurso')

    def test_tipoderecurso_crear(self):
        """"Debe mostrar la pagina para crear tipo de recurso"""
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modrecurso:tiporecurso_create'))
        self.assertEqual(response.status_code, 200, 'error no se pudo crear tipo de recurso')

    def test_tipoderecurso_modificar(self):
        """"Debe mostrar la pagina para modificar tipo de recurso"""
        user = User.objects.get(username='username')
        self.client.login(username='username', password='password')
        tipo = TipoRecurso.objects.all()[:1].get()
        response = self.client.get(reverse_lazy('modrecurso:tiporecurso_update', args={tipo.id}))
        self.assertEqual(response.status_code, 200, 'error no se pudo modificar el tipo de recurso')

    def test_recurso_estado2_max_length(self):
        """Prueba si la maxima longitud de el campo es valida"""
        recurso2 = Recurso.objects.get(nombre='aula01')
        max_length = recurso2._meta.get_field('estado').max_length
        self.assertEquals(max_length, 16,
                          'Fallo la consula a la base de datos al obtener la longitud maxima o esta no es valida')

    def test_tipoderecurso_delete(self):
        """"Debe mostrar la pagina para eliminar tipo de recurso"""
        user = User.objects.get(username='username')
        self.client.login(username='username', password='password')
        tipo = TipoRecurso.objects.all()[:1].get()
        response = self.client.get(reverse_lazy('modrecurso:tiporecurso_delete', args={tipo.id}))
        self.assertEqual(response.status_code, 200, 'error no se pudo eliminar tipo de recurso')

    def test_caracteristicas_tipoderecurso_create(self):
        """"Debe mostrar la pagina para crear caracteristica de recurso"""
        user = User.objects.get(username='username')
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modrecurso:caracteristicas_tiporecurso_create', args={user.id}))
        self.assertEqual(response.status_code, 200, 'error no se pudo crear caracteristica de recurso')

    def test_recurso2_tipo_descripcion_max_length(self):
        """Prueba si la maxima longitud de el campo es valida"""
        recurso2 = Recurso.objects.get(nombre='aula01')
        max_length = recurso2.tipo_recurso._meta.get_field('descripcion').max_length
        self.assertEquals( max_length, 2000,
                          'Fallo la consula a la base de datos al obtener la longitud maxima o esta no es valida')

    def test_caracteristicas_create(self):
        """"Debe mostrar la pagina para crear caracteristica de recurso"""
        user = User.objects.get(username='username')
        response = self.client.get(reverse_lazy('modrecurso:caracteristica_create', args={user.id}))
        self.assertEqual(response.status_code, 200, 'error no se pudo crear caracteristica de recurso')

    def test_recurso2_tipo_nombre_max_length(self):
        """Prueba si la maxima longitud de el campo es valida"""
        recurso2 = Recurso.objects.get(nombre='mesa02')
        max_length = recurso2.tipo_recurso._meta.get_field('nombre').max_length
        self.assertEquals( max_length, 100,
                          'Fallo la consula a la base de datos al obtener la longitud maxima o esta no es valida')

    def test_recurso_estado_max_length(self):
        """Prueba si la maxima longitud de el campo es valida"""
        recurso = Recurso.objects.get(nombre='aula01')
        max_length = recurso._meta.get_field('estado').max_length
        self.assertEquals(max_length, 16,
                          'Fallo la consula a la base de datos al obtener la longitud maxima o esta no es valida')

    def test_recurso_tipo_nombre_max_length(self):
        """Prueba si la maxima longitud de el campo es valida"""
        recurso1 = Recurso.objects.get(nombre='aula01')
        max_length = recurso1.tipo_recurso._meta.get_field('nombre').max_length
        self.assertEquals( max_length, 100,
                          'Fallo la consula a la base de datos al obtener la longitud maxima o esta no es valida')

    def test_recurso_tipo_descripcion_max_length(self):
        """Prueba si la maxima longitud de el campo es valida"""
        recurso1 = Recurso.objects.get(nombre='aula01')
        max_length = recurso1.tipo_recurso._meta.get_field('descripcion').max_length
        self.assertEquals( max_length, 2000,
                          'Fallo la consula a la base de datos al obtener la longitud maxima o esta no es valida')

