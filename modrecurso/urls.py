from django.conf.urls import url
from . import views

app_name = 'modrecurso'

urlpatterns = [
    url(r'^crear/$', views.crear_recurso, name='crear_recurso'),
    url(r'^modificar-(?P<parameter>\d+)/', views.modificar_recurso, name='modificar_recurso'),
    #url(r'^listar/$', views.recurso_list, name='recurso_list'),
    url(r'^eliminar-(?P<pk>\d+)/', views.RecursoDelete.as_view(), name='eliminar_recurso'),
    #url(r'^$', views.recurso_list),
    url(r'^buscar/$', views.recurso_search, name='buscar'),
    url(r'^buscar/reporte_recurso_pdf/$',views.ReporteRecursosPDF.as_view(), name='reporte_recurso_pdf'),
    #url(r'^caracteristicas-(?P<parameter>\d+)/', views.caracteristicas_list, name='caracteristicas_list'),

    # TipoRecurso urls
    # TipoRecurso list
    url(r'^tiporecurso/list/$', views.TipoRecursoList.as_view(), name='tiporecurso_list'),
    # TipoRecurso create
    url(r'^tiporecurso/create/$', views.TipoRecursoCreate.as_view(), name='tiporecurso_create'),
    # TipoRecurso update
    url(r'^tiporecurso/(?P<pk>\d+)/update/$', views.TipoRecursoUpdate.as_view(), name='tiporecurso_update'),
    # TipoRecurso delete
    url(r'^tiporecurso/(?P<pk>\d+)/delete/$', views.TipoRecursoDelete.as_view(), name='tiporecurso_delete'),
    # CaracteristicaTipoRecurso list
    url(r'^tiporecurso/(?P<pk>\d+)/caracteristica/list$', views.caracteristicas_tiporecurso_list, name='caracteristicas_tiporecurso_list'),
    # CaracteristicaTipoRecurso create
    url(r'^tiporecurso/(?P<pk>\d+)/caracteristica/create$', views.CaracteristicaTipoRecursoCreate.as_view(), name='caracteristicas_tiporecurso_create'),

    # Caracteristicas urls
    # Caracteristica create
    url(r'^(?P<pk>\d+)/caracteristica/create/$', views.CaracteristicaCreate.as_view(), name='caracteristica_create'),

    #url(r'^buscar_recurso_list/$', views.buscar_recurso_lista, name='buscar_recurso_list'),
]

