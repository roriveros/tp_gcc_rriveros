from django.db import models
from django.contrib.auth.models import User


class Recurso(models.Model):
    """
    Modelo utilizado para la creacion de recursos de la institucion, asociado con foreignKey a los modelos `modusuario.Area` y `modreserva.Mantenimiento`
    """
    EN_USO = 'EN USO'
    DISPONIBLE = 'DISPONIBLE'
    MANTENIMIENTO = 'EN MANTENIMIENTO'
    DESCOMPUESTO = 'DESCOMPUESTO'
    RESERVADO = 'RESERVADO'
    DESCONOCIDO = 'DESCONOCIDO'
    INUTILIZABLE = 'INUTILIZABLE'
    ESTADO_CHOICES = (
        (EN_USO, 'En Uso'),
        (DISPONIBLE, 'Disponible'),
        (MANTENIMIENTO, 'En Manteminiento'),
        (DESCOMPUESTO, 'Descompuesto'),
        (RESERVADO, 'Reservado'),
        (DESCONOCIDO, 'Desconocido'),
        (INUTILIZABLE, 'Inutilizable'),#todo agregar alguna forma de que pase a este estado
    )
    nombre = models.CharField(max_length=20, unique=True)
    estado = models.CharField(max_length=16, choices=ESTADO_CHOICES, blank=True,default='DISPONIBLE')
    area = models.ForeignKey('modusuario.Area')
    tipo_recurso = models.ForeignKey('TipoRecurso', on_delete=models.CASCADE)

    # import modmantenimiento
    # mantenimiento = models.ForeignKey('modmantenimiento.Mantenimiento')

    # mantenimiento = models.ForeignKey('Mantenimiento')
    # caracteristicas = models.ForeignKey('Caracteristicas', on_delete=models.CASCADE)
    # caracteristicas= models.ManyToManyField('Caracteristicas')

    class Meta:
        permissions = (
            ("list_all_recurso", "Can list all recurso"),
            ("list_recurso", "Can list a certain recurso"),
        )

    def __str__(self):
        #return 'Recurso del tipo {}'.format(self.nombre)
        return self.nombre

    def lista_caracteristicas(self):
        """
        Metodo que retorna un queryset de todas las caracteristicas (model: `modrecurso.Caracteristica`) asociadas al recurso
        """
        recurso = Recurso.objects.get(id=self.id)
        return recurso.caracteristica_set.all()

    def lista_historial(self):
         """
         Metodo que retorna un queryset de todas las caracteristicas (model: `modrecurso.Caracteristica`) asociadas al recurso
         """
         recurso = Recurso.objects.get(id=self.id)
         return recurso.historial_set.all()




class TipoRecurso(models.Model):
    """
    Modelo utilizado para categorizar los recursos (model: `modrecurso.Recurso`) por su tipo
    """
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=2000)

    def __str__(self):
        return self.nombre

    def lista_caracteristicas(self):
        """
        Metodo que retorna un queryset de todas las caracteristicas (model: `modrecurso.CaracteristicaTipoRecurso`) asociadas al tipo de recurso
        """
        tipo_recurso = TipoRecurso.objects.get(id=self.id)
        return tipo_recurso.caracteristicatiporecurso_set.all()

    class Meta:
        permissions = (
            ("list_tiporecurso", "Can list a certain tipo recurso"),
        )

class CaracteristicaTipoRecurso(models.Model):
        """
        Modelo utilizado para agregar detalles o informacion adicional a los tipos de recurso (model: `modrecurso.TipoRecurso`)
        """
        tipo_recurso = models.ForeignKey('TipoRecurso')
        nombre = models.CharField(max_length=2000)

        def __str__(self):
            return self.nombre


# reemplazado por Area de modusuario
# class Departamento(models.Model):
#     nombre = models.CharField(max_length=100)
#     descripcion = models.CharField(max_length=100)
#     def __str__(self):
#         return self.nombre


class Caracteristica(models.Model):
    """
    Modelo utilizado para agregar detalles o informacion adicional a los recursos (model: `modrecurso.Recurso`)
    """
    recurso = models.ForeignKey('Recurso')
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=2000)

    def __str__(self):
        return self.nombre

# class Mantenimiento(models.Model):
#     fechaInicio = models.DateField(blank=False)
#     fechaFin = models.DateField(blank=False)
#     motivo = models.CharField(max_length=100)
#     descripcion = models.CharField(max_length=100)
#     costo= models.FloatField(max_length=100)
#     PREVENTIVO='PREVENTIVO'
#     CORRECTIVO='CORRECTIVO'
#     TIPO_CHOICES = (
#         (PREVENTIVO, 'Preventivo'),
#         (CORRECTIVO, 'Correctivo'),
#     )
#     tipo = models.CharField(
#         max_length=10,
#         choices=TIPO_CHOICES,
#         blank=True
#     )
#     DISPONIBLE = 'DISPONIBLE'
#     DESCOMPUESTO = 'DESCOMPUESTO'
#     RESULTADO_CHOICES = (
#         (DISPONIBLE, 'Disponible'),
#         (DESCOMPUESTO, 'Descompuesto'),
#     )
#     resultado = models.CharField(
#         max_length=12,
#         choices=RESULTADO_CHOICES,
#         blank=True
#     )
#     def __str__(self):
#         return self.tipo
#
