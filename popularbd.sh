#! /bin/bash
# Variables utilizadas durante el script

db_user=admin
db_pass=admin
db_name=dubium

if [ "$(sudo -su postgres psql -tAc "SELECT 1 FROM pg_database WHERE datname='$db_name'" )" = '1' ]
then
    echo "Database already exists"
    sudo -u postgres psql -c "DROP DATABASE $db_name;"
fi

sudo -u postgres psql -c "CREATE DATABASE $db_name;"

if [ "$(sudo -su postgres psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='$db_user'" )" = '1' ]
then
    echo "User already exists"
    sudo -u postgres psql -c "DROP USER $db_user;"
fi

sudo -u postgres psql -c "CREATE USER $db_user WITH PASSWORD '$db_pass';"
sudo -u postgres psql -c "ALTER ROLE $db_user SET client_encoding TO 'utf8';"
sudo -u postgres psql -c "ALTER ROLE $db_user SET default_transaction_isolation TO 'read committed';"
sudo -u postgres psql -c "ALTER ROLE $db_user SET timezone TO 'UTC';"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE $db_name TO $db_user;"
sudo -u postgres psql -c "ALTER USER $db_user CREATEDB;"

rm -rf modusuario/migrations

rm -rf modrecurso/migrations

rm -rf modmantenimiento/migrations

rm -rf modreserva/migrations

rm -rf modpublico/migrations


python manage.py makemigrations modusuario

python manage.py makemigrations modrecurso


rm -rf modusuario/migrations/0001_initial.py

rm -rf modrecurso/migrations/0001_initial.py

python manage.py makemigrations modusuario

python manage.py makemigrations modmantenimiento

python manage.py makemigrations modpublico

python manage.py makemigrations modreserva

python manage.py migrate

python manage.py shell < datos.py
