from django.db import models
from django.conf import settings
from modrecurso.models import TipoRecurso


class Profile(models.Model):
    """
    Almacena un Perfil asociado a un Usuario. Se utiliza para agregar mas campos e informacion a `auth.User` utilizando una relacion OneToOne.
    """

    # Titular de Cátedra, Adjunto, 	Asistente, Encargado de Catedra, Auxiliar, Alumno, funcionario.
    TITULAR = 'TITULAR DE CATEDRA'
    ADJUNTO = 'ADJUNTO'
    ASISTENTE = 'ASISTENTE'
    ENCARGADO = 'ENCARGADO DE CATEDRA'
    AUXILIAR = 'AUXILIAR'
    ALUMNO = 'ALUMNO'
    FUNCIONARIO = 'FUNCIONARIO'
    CATEGORIA_CHOICES = (
        (TITULAR, 'Titular de Catedra'),
        (ADJUNTO, 'Adjunto'),
        (ASISTENTE, 'Asistente'),
        (ENCARGADO, 'Encargado de Catedra'),
        (AUXILIAR, 'Auxiliar'),
        (ALUMNO, 'Alumno'),
        (FUNCIONARIO, 'Funcionario'),
    )

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='usuario')
    categoria = models.CharField(max_length=20, choices=CATEGORIA_CHOICES, blank=True)
    telefono = models.CharField(max_length=30, blank=True, null=True)
    ci = models.CharField(max_length=10, blank=True, null=True)
    direccion = models.CharField(max_length=200, blank=True)
    tipo_recurso = models.ManyToManyField('modrecurso.TipoRecurso', blank=True)
    area = models.ForeignKey('Area', blank=True, null=True)

    def __str__(self):
        #return 'Perfil para el usuario {}'.format(self.user.username)
        return (self.user.username)

    class Meta:
        permissions = (
            ("list_user", "Can list certain user"),
            ("can_edit_config", "Can edit configuration"),
        )

class Area(models.Model):
    """
    Modelo utilizado para crear un Area de la institucion
    """
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=2000)

    def __str__(self):
        return self.nombre

    class Meta:
        permissions = (
            ("list_area", "Can list certain area"),
            ("list_rol", "Can list certain rol"),
        )
