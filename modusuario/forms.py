from django.contrib.auth.models import User, Group
from django import forms
from .models import Profile, Area
from captcha.fields import ReCaptchaField
from dubium import settings
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _


class UserRegistrationForm(forms.ModelForm):
    """
        Formulario que se utiliza para la registracion de nuevos usuarios.
        Despliega campos relacionados al model `auth.User`
    """
    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        super(UserRegistrationForm, self).__init__(*args, **kwargs)
        # there's a 'fields' property now
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

    #rol = forms.ModelChoiceField(label='Rol', queryset=Group.objects.all(), required=True)
    password = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Repetir contraseña', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password', 'password2', 'groups')
        labels = {
            'groups': 'Rol'
        }
        help_texts = {
            'groups': 'Los roles que tiene asignado este usuario. Un usuario tendrá todos los permisos asignados de cada uno de sus roles.'
        }

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('Las contraseñas no coinciden.')
        return cd['password2']

class ProfileRegistrationForm(forms.ModelForm):
    """
        Formulario que se utiliza para la registracion de nuevos usuarios.
        Despliega campos relacionados al modelo `modusuario.Profile`
    """
    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        super(ProfileRegistrationForm, self).__init__(*args, **kwargs)
        # there's a 'fields' property now
        self.fields['categoria'].required = True

    captcha = ReCaptchaField()

    class Meta:
        model = Profile
        fields = ('categoria', 'area', 'tipo_recurso', 'telefono', 'ci', 'direccion', 'captcha')


class UserPersonalEditForm(forms.ModelForm):
    """
        Formulario que se utiliza para que un usuario pueda cambiar su informacion personal relacionado al modelo `auth.User`.
        Despliega campos relacionados al model `auth.User`
    """

    # password_new = forms.CharField(label='Nueva Contraseña', widget=forms.PasswordInput, required=False)
    # password_repeat = forms.CharField(label='Repetir contraseña', widget=forms.PasswordInput, required=False)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')

    def clean_password_repeat(self):
        cd = self.cleaned_data
        if cd['password_new'] != cd['password_repeat']:
            raise forms.ValidationError('Las contraseñas no coinciden.')
        return cd['password_repeat']

class ProfilePersonalEditForm(forms.ModelForm):
    """
        Formulario que se utiliza para que un usuario pueda cambiar su informacion personal relacionado al modelo `modusuario.Profile`.
        Despliega campos relacionados al model `modusuario.Profile`
    """
    class Meta:
        model = Profile
        fields = ('telefono', 'ci', 'direccion')

class MainConfigurationForm(forms.Form):
    """
        Formulario que se utilizará para la edicion de menu_recurso.html del sistema.

    """
    conf_email_use_tls = forms.BooleanField(label='Email use tls:', required=True)
    conf_email_host = forms.CharField(label='Email host:', max_length=1024, required=True)
    conf_email_host_user = forms.EmailField(label='Email host user:',max_length=255, required=True)
    conf_email_host_password = forms.CharField(label='Email host password:', widget=forms.PasswordInput, max_length=255, required=False)
    conf_email_port = forms.IntegerField(label='Email port:', required=True)
    fecha_reserva=forms.IntegerField(label='Tiempo para resolver reservas:', required=True,help_text='dias')
    tiempo_antes = forms.IntegerField(label='Tiempo previo para retiro de recurso:', required=True,help_text='minutos')
    tiempo_despues = forms.IntegerField(label='Tiempo posterior para retiro de recurso:', required=True,help_text='minutos')


class UserEditForm(forms.ModelForm):
    """
        Formulario que se utiliza para la edicion de  usuarios.
        Despliega campos relacionados al modelo `auth.User`
    """
    password_new = forms.CharField(label='Nueva Contraseña', widget=forms.PasswordInput, required=False)
    password_repeat = forms.CharField(label='Repetir contraseña', widget=forms.PasswordInput, required=False)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'password_new', 'password_repeat', 'email', 'groups', 'is_active')
        labels = {
            'groups': 'Roles'
        }

    def clean_password_repeat(self):
        cd = self.cleaned_data

        if cd['password_new'] != cd['password_repeat']:
            raise forms.ValidationError('Las contraseñas no coinciden.')

        return cd['password_repeat']

class ProfileEditForm(forms.ModelForm):
    """
        Formulario que se utiliza para la edicion de usuarios.
        Despliega campos relacionados al model `modusuario.Profile`
    """

    class Meta:
        model = Profile
        fields = ('categoria', 'area', 'tipo_recurso', 'telefono', 'ci', 'direccion')


class UserCreateForm(forms.ModelForm):
    """
        Formulario que se utiliza para la creacion de nuevos usuarios.
        Despliega campos relacionados al modelo`auth.User`
    """
    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        super(UserCreateForm, self).__init__(*args, **kwargs)
        # there's a 'fields' property now
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

    # rol = forms.ModelMultipleChoiceField(label='Rol', queryset=Group.objects.all(), required=True)
    password = forms.CharField(label='Contraseña', widget=forms.PasswordInput, required=True)
    password2 = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput, required=True)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password', 'password2', 'groups', 'is_active')
        labels = {
            'groups': 'Roles'
        }
        help_texts = {
            'groups': 'Los roles que tiene asignado este usuario. Un usuario tendrá todos los permisos asignados de cada uno de sus roles.'
        }

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('Las contraseñas no coinciden.')
        return cd['password2']

class ProfileCreateForm(forms.ModelForm):
    """
        Formulario que se utiliza para la creacion de nuevos usuarios.
        Despliega campos relacionados con el modelo `modusuario.Profile`
    """
    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        super(ProfileCreateForm, self).__init__(*args, **kwargs)
        # there's a 'fields' property now
        self.fields['categoria'].required = True

    class Meta:
        model = Profile
        fields = ('categoria', 'area', 'tipo_recurso', 'telefono', 'ci', 'direccion')

class FormFiltrosUsuario(forms.Form):

    nombre_usuario = forms.CharField(required=False)
    nombre = forms.CharField(required=False)
    apellido = forms.CharField(required=False)
    email = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        super(FormFiltrosUsuario, self).__init__(*args, **kwargs)
        CATEGORIA_CHOICES = (('', 'Todos'),) + Profile.CATEGORIA_CHOICES
        ESTADO_CHOICES = (('', 'Todos'),
                          (True, 'Activado'),
                          (False, 'Desactivado'),
                          )
        self.fields['estado'] = forms.ChoiceField(required=False, choices=ESTADO_CHOICES)
        self.fields['categoria'] = forms.ChoiceField(required=False, choices=CATEGORIA_CHOICES)
        self.fields['area'] = forms.ModelChoiceField(required=False, queryset=Area.objects.all(), empty_label='Todas')
