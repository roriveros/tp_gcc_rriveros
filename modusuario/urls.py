from django.contrib.auth import views as auth_views
from django.conf.urls import url
from . import views

app_name = 'modusuario'

urlpatterns = [
    # previous login view
    # url(r'^login/$', views.user_login, name='login'),

    # login / logout urls
    url(r'^login/$', auth_views.login,  name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^logout-then-login/$', auth_views.logout_then_login, name='logout_then_login'),

    # dashboard
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    # dashboard
    url(r'^$', views.home, name='home'),

    # change password urls
    url(r'^password-change/$', auth_views.password_change, {'post_change_redirect': 'modusuario:password_change_done'}, name='password_change'),
    url(r'^password-change/done/$', auth_views.password_change_done, name='password_change_done'),

    # restore password urls
    url(r'^password-reset/$', auth_views.password_reset, {'post_reset_redirect': 'modusuario:password_reset_done'}, name='password_reset'),
    url(r'^password-reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^password-reset/confirm/(?P<uidb64>[-\w]+)/(?P<token>[-\w]+)/$', auth_views.password_reset_confirm, {'post_reset_redirect': 'modusuario:password_reset_complete'}, name='password_reset_confirm'),
    url(r'^password-reset/complete/$', auth_views.password_reset_complete, name='password_reset_complete'),

    # register user
    url(r'^register/$', views.user_register, name='register'),

    # edit user propio
    url(r'^edit/$', views.user_detail_edit, name='edit'),

    # edit parameters
    url(r'^parameters/$', views.email_config_edit, name='configuracion'),

    # user list
    #url(r'^usuario/list/$', views.UserList.as_view(), name='listar_usuarios'),
    url(r'^usuario/list/$', views.user_list, name='listar_usuarios'),
    # user detail
    url(r'^usuario/(?P<pk>\d+)/$', views.UserDetail.as_view(), name='detalle_usuario'),
    # user create
    url(r'^usuario/create/$', views.user_create, name='crear_usuario'),
    # user update
    url(r'^usuario/(?P<pk>\d+)/update/$', views.user_update, name='actualizar_usuario'),
    # user delete
    url(r'^usuario/(?P<pk>\d+)/delete/$', views.UserDelete.as_view(), name='eliminar_usuario'),

    # area list
    url(r'^area/list/$', views.AreaList.as_view(), name='area_list'),
    # area create
    url(r'^area/create/$', views.AreaCreate.as_view(), name='area_create'),
    # area update
    url(r'^area/(?P<pk>\d+)/update/$', views.AreaUpdate.as_view(), name='area_update'),
    # area delete
    url(r'^area/(?P<pk>\d+)/delete/$', views.AreaDelete.as_view(), name='area_delete'),

    # rol list
    url(r'^rol/list/$', views.RolList.as_view(), name='rol_list'),
    # rol create
    url(r'^rol/create/$', views.RolCreate.as_view(), name='rol_create'),
    # rol update
    url(r'^rol/(?P<pk>\d+)/update/$', views.RolUpdate.as_view(), name='rol_update'),
    # rol delete
    url(r'^rol/(?P<pk>\d+)/delete/$', views.RolDelete.as_view(), name='rol_delete'),
]