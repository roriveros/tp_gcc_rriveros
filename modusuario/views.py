from django.core.mail import send_mail
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from django.dispatch import receiver
from django.contrib.auth.forms import PasswordChangeForm
from django.views.generic import ListView, DetailView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth import update_session_auth_hash
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from .forms import UserRegistrationForm, ProfileRegistrationForm, ProfilePersonalEditForm, UserPersonalEditForm, \
    MainConfigurationForm, UserEditForm, ProfileEditForm, UserCreateForm, ProfileCreateForm, FormFiltrosUsuario
from .models import Area, Profile

from modusuario.email_config import conf_email_host, conf_email_host_user, conf_email_host_password, conf_email_port, \
    conf_email_use_tls,fecha_reserva,tiempo_despues,tiempo_antes


def user_register(request):
    """
    Muestra formularios en base a los datos de los modelos `auth.User` y `modusuario.Profile` 

    **Context**

    ``new_profile``
        Una instancia del modelo`modusuario.Profile` 

    ``new_user``
        Una instancia del modelo`auth.User` 

    **Template:**

    `modusuario/register.html`

    """

    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        profile_form = ProfileRegistrationForm(request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            # Create a new user object but avoid saving it yet
            new_user = user_form.save(commit=False)
            # Set the chosen password
            new_user.set_password(user_form.cleaned_data['password'])

            new_user.is_active = False
            # Save the User object
            new_user.save()
            user_form.save_m2m()

            # Create the user profile
            new_profile = profile_form.save(commit=False)
            new_profile.user = new_user
            new_profile.save()
            profile_form.save_m2m()

            send_mail(
                'Creacion de cuenta Dubium',
                'Su cuenta ha sido creada exitosamente, por favor, espere a que el Administrador confirme su cuenta.',
                'dubium@example.com',
                [new_user.email],
                fail_silently=False,
            )

            return render(request, 'modusuario/register_done.html', {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
        profile_form = ProfileRegistrationForm()
    return render(request, 'modusuario/register.html', {'user_form': user_form, 'profile_form': profile_form})


@login_required
def dashboard(request):
    """
    Muestra el dashboard del usuario.


    """
    return render(request, 'inicio.html', {'section': 'dashboard'})


@login_required
def user_detail_edit(request):
    """
    Muestra campos de los modelos `auth.User` y `modusuario.Profile`  del propio para ser editados.


    """
    if request.method == 'POST':
        user_form = UserPersonalEditForm(instance=request.user, data=request.POST)
        profile_form = ProfilePersonalEditForm(instance=request.user.profile, data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Perfil actualizado exitosamente')
        else:
            messages.error(request, 'Error al actualizar su perfil')
    else:
        user_form = UserPersonalEditForm(instance=request.user)
        profile_form = ProfilePersonalEditForm(instance=request.user.profile)
    return render(request, 'modusuario/edit.html', {'user_form': user_form, 'profile_form': profile_form})


@login_required
def home(request):
    """
    Vista que muestra la pagina de inicio
    """
    return render(request, "inicio.html", {'section': 'home'})


def index(request):
    """
    Vista que redirecciona al login o a la pagina de inicio si se esta autenticado
    """

    if request.user.is_authenticated():
        return redirect(reverse_lazy('modusuario:home'))
    else:
        return redirect(reverse_lazy('modusuario:login'))


@login_required
def email_config_edit(request):
    """
    Vista que sirve para editar la configuracion de email de settings.py
    """

    if request.method == 'POST':
        form = MainConfigurationForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            # ahora en cd, se tiene el form como diccionario.
            b = cd.get('conf_email_use_tls')
            c = cd.get('conf_email_host')
            d = cd.get('conf_email_host_user')
            e = cd.get('conf_email_host_password')
            f = cd.get('conf_email_port')
            g = cd.get('fecha_reserva')
            h = cd.get('tiempo_antes')
            i = cd.get('tiempo_despues')

            # Creo mi archivo con la funcion generate
            def generate_email_config():
                config = open('modusuario/email_config.py', 'w')
                config.close()

            generate_email_config()

            def change_email_config():
                config = open('modusuario/email_config.py', 'a')
                config.write('conf_email_use_tls= "%s"\n' % b)
                config.write('conf_email_host = "%s"\n' % c)
                config.write('conf_email_host_user = "%s"\n' % d)
                config.write('conf_email_host_password = "%s"\n' % e)
                config.write('conf_email_port = "%d"\n' % f)
                config.write('fecha_reserva = "%d"\n' % g)
                config.write('tiempo_antes = "%d"\n' % h)
                config.write('tiempo_despues = "%d"\n' % i)
                config.close()

            change_email_config()

            messages.success(request, 'Configuración del sistema actualizada exitosamente')
        else:
            messages.error(request, 'Error al actualizar la configuración del sistema')
    else:
        form = MainConfigurationForm(initial={'conf_email_use_tls': conf_email_use_tls,
                                              'conf_email_host': conf_email_host,
                                              'conf_email_host_user': conf_email_host_user,
                                              'conf_email_host_password': conf_email_host_password,
                                              'conf_email_port': conf_email_port,
                                              'fecha_reserva': fecha_reserva,
                                              'tiempo_antes': tiempo_antes,
                                              'tiempo_despues': tiempo_despues})
    return render(request, 'modusuario/change_parameters.html', {'form': form})



def user_list(request):
    valores = {  # aca se van poniendo los valores para filtrar el query
    }
    arguementos = {}
    form_filtros = FormFiltrosUsuario(request.GET)
    if form_filtros.is_valid():
        cd = form_filtros.cleaned_data
        valores['username__icontains'] = cd['nombre_usuario']
        valores['first_name__icontains'] = cd['nombre']
        valores['last_name__icontains'] = cd['apellido']
        valores['email__icontains'] = cd['email']
        valores['is_active'] = cd['estado']
        valores['profile__categoria'] = cd['categoria']
        valores['profile__area'] = cd['area']


    for k, v in valores.items():
        if v:
            arguementos[k] = v
    pagreserva_list = User.objects.filter(**arguementos).order_by('username')

    page = request.GET.get('page', 1)
    paginator = Paginator(pagreserva_list, 15)

    try:
        lista_usuarios = paginator.page(page)
    except PageNotAnInteger:
        lista_usuarios = paginator.page(1)
    except EmptyPage:
        lista_usuarios = paginator.page(paginator.num_pages)
    return render(request, 'modusuario/user_list.html', {'lista_usuarios': lista_usuarios, 'form': form_filtros})


class UserList(ListView):
    """
    Vista que sirve para listar todos los usuarios del model `auth.User`
    """
    model = User
    paginate_by = 15
    context_object_name = 'lista_usuarios'
    template_name = 'modusuario/user_list.html'
    ordering = ['username']


class UserDetail(DetailView):
    """
    Vista que sirve para mostrar detalles de `auth.User`
    """
    model = User
    context_object_name = 'usuario'
    template_name = 'modusuario/user_detail.html'


class UserCreate(CreateView):
    """
    Vista que sirve para crear un `auth.User`
    """
    model = User
    fields = ['username', 'first_name', 'last_name', 'email', 'password', 'groups', 'is_active']
    template_name = 'modusuario/user_create.html'

    def get_success_url(self):
        return reverse('modusuario:listar_usuarios')


@login_required
def user_update(request, pk):
    """
    Vista que sirve para actualizar un `auth.User`
    """
    user = User.objects.get(id=pk)
    if request.method == 'POST':

        user_form = UserEditForm(instance=user, data=request.POST)
        # password_form = PasswordChangeForm(user, request.POST)
        profile_form = ProfileEditForm(instance=user.profile, data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            #todo Mejorar la forma de cambiar el password, si el propio usuario cambia su password deberia actualizar su sesion o enviarlo al login
            user = user_form.save(commit=False)
            password = user_form.cleaned_data['password_new']
            if(password != ''):
                print(password)
                user.set_password(password)
            user.save()
            user_form.save_m2m()
            profile_form.save()
            # user = password_form.save()
            # update_session_auth_hash(request, user)  # Importante!
            #            messages.success(request, 'Usuario actualizado exitosamente')
            return redirect('modusuario:listar_usuarios')
        else:
            messages.error(request, 'Error al actualizar el usuario')
    else:
        user_form = UserEditForm(instance=user)
        profile_form = ProfileEditForm(instance=user.profile)
    return render(request, 'modusuario/user_update.html', {'user_form': user_form, 'profile_form': profile_form, 'username': user.username})


class UserDelete(DeleteView):
    """
    Vista que sirve para eliminar un `auth.User`
    """
    model = User
    template_name = 'modusuario/user_confirm_delete.html'

    def get_success_url(self):
        return reverse('modusuario:listar_usuarios')


class AreaList(ListView):
    """
    Vista que sirve para listar las areas del model `modusuario.Area`
    """
    model = Area
    paginate_by = 15
    context_object_name = 'area_list'
    template_name = 'modusuario/area_list.html'
    ordering = ['id']


class AreaCreate(CreateView):
    """
    Vista que sirve para crear un `modusuario.Area`
    """
    model = Area
    fields = ['nombre', 'descripcion']
    template_name = 'modusuario/area_create.html'

    def get_success_url(self):
        return reverse('modusuario:area_list')


class AreaUpdate(UpdateView):
    """
    Vista que sirve para actualizar un `modusuario.Area`
    """
    model = Area
    fields = ['nombre', 'descripcion']
    template_name = 'modusuario/area_update.html'

    def get_success_url(self):
        return reverse('modusuario:area_list')


class AreaDelete(DeleteView):
    """
    Vista que sirve para eliminar un `modusuario.Area`
    """
    model = Area

    def get_success_url(self):
        return reverse('modusuario:area_list')


@login_required()
def user_create(request):
    """
    Muestra formularios en base a los datos de los modelos `auth.User` y `modusuario.Profile` 

    **Context**

    ``new_profile``
        Una instancia del modelo`modusuario.Profile` 

    ``new_user``
        Una instancia del modelo`auth.User` 


    """
    if request.method == 'POST':
        user_form = UserCreateForm(request.POST)
        profile_form = ProfileCreateForm(request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            # Create a new user object but avoid saving it yet
            new_user = user_form.save(commit=False)
            # Set the chosen password
            new_user.set_password(user_form.cleaned_data['password'])

            # Save the User object
            new_user.save()
            user_form.save_m2m()

            # Create the user profile
            new_profile = profile_form.save(commit=False)
            new_profile.user = new_user
            new_profile.save()
            profile_form.save_m2m()

            return redirect('modusuario:listar_usuarios')
    else:
        user_form = UserCreateForm()
        profile_form = ProfileCreateForm()
    return render(request, 'modusuario/user_create.html', {'user_form': user_form, 'profile_form': profile_form})

class RolList(ListView):
    """
    Vista que sirve para listar las areas del model `auth.Group`
    """
    model = Group
    paginate_by = 15
    context_object_name = 'rol_list'
    template_name = 'modusuario/rol_list.html'
    ordering = ['id']


class RolCreate(CreateView):
    """
    Vista que sirve para crear un `auth.Group`
    """
    model = Group
    fields = ['name', 'permissions']
    template_name = 'modusuario/rol_create.html'

    def get_success_url(self):
        return reverse('modusuario:rol_list')


class RolUpdate(UpdateView):
    """
    Vista que sirve para actualizar un `auth.Group`
    """
    model = Group
    fields = ['name', 'permissions']
    template_name = 'modusuario/rol_update.html'

    def get_success_url(self):
        return reverse('modusuario:rol_list')


class RolDelete(DeleteView):
    """
    Vista que sirve para eliminar un `auth.Group`
    """
    model = Group
    template_name = 'modusuario/rol_confirm_delete.html'

    def get_success_url(self):
        return reverse('modusuario:rol_list')

@receiver(pre_save, sender=User)
def do_something_if_user_changed(sender, instance, **kwargs):
    try:
        obj = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        pass # Object is new, so field hasn't technically changed, but you may want to do something else here.
    else:
        if obj.is_active != instance.is_active: # Field has changed
            # do something
            if instance.is_active == True:
                send_mail(
                    'Confirmacion de cuenta Dubium',
                    'Su cuenta ha sido confirmada por el Administrador, ya puede utilizar el sistema.',
                    'dubium@example.com',
                    [instance.email],
                    fail_silently=False,
                )
            else:
                send_mail(
                    'Desactivacion de cuenta Dubium',
                    'Su cuenta ha sido desactivada por el Administrador.',
                    'dubium@example.com',
                    [instance.email],
                    fail_silently=False,
                )
