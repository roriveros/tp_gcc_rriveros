from django.contrib import admin
from .models import Profile, Area
#from .forms import MainConfigurationForm
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'categoria', 'telefono', 'ci', 'direccion')
    search_fields = ['user__username', 'categoria']
    list_filter = ['categoria']
    ordering = ('user__username',)


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Area)


#class MainConfigurationAdmin(admin.formAdmin):
#    list_display = ['']
#admin.site.register(MainConfigurationForm, MainConfigurationAdmin)