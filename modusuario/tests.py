from django.test import TestCase
from django.contrib.auth.models import User
from .models import Profile, Area
from django.core.urlresolvers import reverse_lazy


class ViewsTestCase(TestCase):
    """Clase para pruebas unitarias de las vistas del modulo usuario"""

    def setUp(self):
        """Creacion de los objetos usuario y perfil del usuario para las pruebas"""
        user = User.objects.create_user('username', 'username@gmail.com', 'password')
        profile = Profile.objects.create(user=user, direccion='La esquina', ci='1.234.567')

    def test_home_anonymous_user(self):
        """"Debe redireccionar al login"""
        response = self.client.get(reverse_lazy('modusuario:home'))
        self.assertEqual(response.status_code, 302)

    def test_home_logged_user(self):
        """"Debe mostrar la pagina menu principal (home)"""
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modusuario:home'))
        self.assertEqual(response.status_code, 200)


    def test_index_anonymous_user(self):
        """"Debe redireccionar al login"""
        response = self.client.get(reverse_lazy('index'))
        self.assertEqual(response.status_code, 302)

    def test_index_logged_user(self):
        """"Debe redireccionar al menu principal (home)"""
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('index'))
        self.assertEqual(response.status_code, 302)

    def test_edit_anonymous_user(self):
        """"Debe redireccionar al login"""
        response = self.client.get(reverse_lazy('modusuario:edit'))
        self.assertEqual(response.status_code, 302)

    def test_edit_logged_user(self):
        """"Debe mostrar la pagina edit"""
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modusuario:edit'))
        self.assertEqual(response.status_code, 200, 'error no se pudo editar el usuario logged')

    def test_change_parameters(self):
        """"Debe mostrar la pagina para cambiar parametros"""
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modusuario:configuracion'))
        self.assertEqual(response.status_code, 200, 'error no se pudo editar los parametros de configuracion')

    def test_user_create(self):
        """"Debe mostrar la pagina para crear usuarios"""
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modusuario:crear_usuario'))
        self.assertEqual(response.status_code, 200, 'error no se pudo crear usuario')

    def test_user_update(self):
        """"Debe mostrar la pagina para actualizar usuarios"""
        user = User.objects.get(username = 'username')
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modusuario:actualizar_usuario', args = {user.id}))
        self.assertEqual(response.status_code, 200, 'error no se pudo actualizar usuario')

    def test_user_confirm_delete(self):
        """"Debe mostrar la pagina para eliminar usuarios"""
        user = User.objects.get(username = 'username')
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modusuario:eliminar_usuario', args = {user.id}))
        self.assertEqual(response.status_code, 200, 'error no se pudo eliminar usuario')

    def test_user_detail(self):
        """"Debe mostrar la pagina para eliminar usuarios"""
        user = User.objects.get(username = 'username')
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modusuario:detalle_usuario', args = {user.id}))
        self.assertEqual(response.status_code, 200, 'error no se pudo mostrar detalle de usuario')

    def test_area_create(self):
        """"Debe mostrar la pagina para crear area"""
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modusuario:area_create'))
        self.assertEqual(response.status_code, 200, 'error no se pudo crear area')

    def test_area_update(self):
        """"Debe mostrar la pagina para actualizar area"""
        user = User.objects.get(username = 'username')
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modusuario:area_update', args = {user.id}))
        self.assertEqual(response.status_code, 404, 'error no se pudo actualizar area')

    def test_area_delete(self):
        """"Debe mostrar la pagina para borrar area"""
        user = User.objects.get(username = 'username')
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modusuario:area_delete', args = {user.id}))
        self.assertEqual(response.status_code, 404, 'error no se pudo borrar area')

    def test_rol_create(self):
        """"Debe mostrar la pagina para crear rol"""
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modusuario:rol_create'))
        self.assertEqual(response.status_code, 200, 'error no se pudo crear rol')

    def test_rol_update(self):
        """"Debe mostrar la pagina para actualizar rol"""
        user = User.objects.get(username = 'username')
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modusuario:rol_update', args = {user.id}))
        self.assertEqual(response.status_code, 404, 'error no se pudo actualizar rol')

    def test_rol_delete(self):
        """"Debe mostrar la pagina para borrar rol"""
        user = User.objects.get(username = 'username')
        self.client.login(username='username', password='password')
        response = self.client.get(reverse_lazy('modusuario:rol_delete', args = {user.id}))
        self.assertEqual(response.status_code, 404, 'error no se pudo borrar rol')

